---
layout: secure_and_protect_direction
title: Product Section Direction - Security
description: "Security visibility from development to operations to minimize risk"
canonical_path: "/direction/security/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

<p align="center">
    <font size="+2">
        <b>Security visibility from development to operations to minimize risk</b>
    </font>
</p>

GitLab provides the single application organizations need to find, triage, and fix vulnerabilities as well as protect
applications, services, and cloud-native environments. This enables organizations to proactively manage their overall
security risk.

This empowers organizations as they can apply repeatable, defensible processes that automate security and compliance
policies from development to production.

<%= devops_diagram(["Secure","Protect"]) %>

## Section Overview

The Sec Section focuses on providing security visibility across the entire software development lifecycle and is
comprised of the [Secure](https://about.gitlab.com/direction/secure/) and [Protect](https://about.gitlab.com/direction/protect/)
stages of the DevOps lifecycle. This is accomplished by
[shifting security testing left](https://about.gitlab.com/direction/security/#shift-left-no-more-left-than-that) with
the Secure stage enabling developers to begin security scanning with their first written line of code. As applications
and application updates are moved into production, the Protect stage provides SecOps and Ops teams the ability to
mitigate attacks targeting the applications in cloud-native environments. The combination of both stages provides
organizations with visibility into their security risk from the first line of code written to applications
deployed within production.

### GitLab and the DevSecOps Lifecycle

GitLab is uniquely positioned to fully support DevSecOps by providing a single application for the entire software development lifecycle. This includes both shifting application security testing (AST) left as well as protection capabilities for applications running within production.

![DevSecOps Lifecycle](/images/direction/sec/DevSecOps_Lifecycle.png)

GitLab’s single application completely maps directly to the DevSecOps lifecycle. The Secure Stage provides the Develop, Analyze, and Mitigate portions of the DevSecOps lifecycle, while GitLab's Protect stage provides the Protect portion.  Together, GitLab supports all teams involved in delivering secure applications:

* Develop: Developers create new source code, including new features and bug fixes, and commit this code to a branch within the project. This step is supported by the [Create](https://about.gitlab.com/stages-devops-lifecycle/create/) stage of the DevOps lifecycle providing developers with [source code management](https://about.gitlab.com/product/source-code-management/), [code editors](https://about.gitlab.com/direction/create/web_ide/), and [code review](https://about.gitlab.com/product/code-review/) workflows.
* Analyze: Upon code commit, Secure scanners are automatically started and identify any new security findings with the delta code change. This enables developers to stay within context enabling them to understand the cause and effect of their code change. Secure scanners leverage the [Verify](https://about.gitlab.com/stages-devops-lifecycle/verify/) stage of the DevOps lifecycle to provide scanning within the [CI pipeline](https://about.gitlab.com/product/continuous-integration/).
* Mitigate: Developers are provided with the [details needed](https://docs.gitlab.com/ee/user/application_security/index.html#interacting-with-the-vulnerabilities) to understand how to remediate the newly introduced security findings. Developers are also offered [automatic remediation](https://docs.gitlab.com/ee/user/application_security/#automatic-remediation-for-vulnerabilities) where applicable.
* Protect: Security and operations teams can protect applications once they are deployed into cloud-native environments. This includes setting [firewall rules](https://docs.gitlab.com/ee/user/project/clusters/protect/container_network_security/) and [blocking attacks](https://docs.gitlab.com/ee/user/project/clusters/protect/container_host_security/) as well as providing visibility via [security orchestration with policy enforcement and alert dashboards](https://docs.gitlab.com/ee/user/application_security/threat_monitoring/).

### Lowering the Cost of Remediation

The earlier a security vulnerability can be remediated has both risk reduction and cost reduction benefits.

![Cost of Remediation](/images/direction/sec/Cost_Of_Remediation.png)

When security vulnerabilities are identified at the time of code commit, developers can understand how their newly
introduced code has led to this new issue. This gives the developer a cause-and-effect enabling quicker resolution
while not having the time hit of context switching. This is not true as security scanning is performed later in the
software development lifecycle. New vulnerabilities may not be identified until weeks or months after they were added
to the application while under development.

Time is not the only savings when shifting security left.

![Stage of Remediation](/images/direction/sec/Stage_of_Remediation.png)

In [“The Economic Impacts of Inadequate Infrastructure for Software Testing”](https://www.nist.gov/system/files/documents/director/planning/report02-3.pdf), NIST estimated the cost of remediating software bugs at $59.5 billion/year. This is compounded when taking in the average time to remediate software bugs. In [“Software Development Price Guide & Hourly Rate Comparison”](https://www.fullstacklabs.co/blog/software-development-price-guide-hourly-rate-comparison), FullStack Labs estimates the average cost of a software developer at $300/hour. The following table outlines the cost to remediate software bugs at different stages of the software development lifecycle:

![Completed Loop](/images/direction/sec/costs_table.png)

These costs are just the start of the financial impact when the software bug is also a software vulnerability. IBM, in partnership with the Ponemon Institute, put the [average cost to remediate a data breach in 2020](https://www.ibm.com/security/digital-assets/cost-data-breach-report/#/) at $3.86 million (USD). This does not take into consideration the reputation impact to the organization.

### Closing the Loop

Having visibility into security risk in just development only provides you with half of the picture. Development and
SecOps teams need to have a closed feedback loop enabling both teams to be successful. Development teams can gain
insight into attacks targeting the applications they develop. This allows them to prioritize vulnerabilities correctly,
enabling proactive resolutions to reduce risk. Likewise, SecOps teams can gain insight from their development
counterparts, providing them with visibility into how the application works. This allows them to best apply proactive
measures to mitigate attacks targeting the application until development can fix the vulnerability.

![Completed Loop](/images/direction/sec/Completed_Loop.png)

Closing the loop requires close collaboration, transparency, and efficiencies that only a single platform for the entire DevOps lifecycle can provide. Shifting security left while also providing protection for applications in production within a single application empowers teams to work closer together. [Security is a team sport](https://ddesanto-update-sec-direction-fy21q4.about.gitlab-review.app/direction/security/#security-is-a-team-effort) and teams working together can best reduce their organization’s overall security risk.

### Groups

The Security Section is made up of two DevOps stages, Secure and Protect, and eight groups supporting the major categories of DevSecOps including:

* **Static Analysis** - Assess your applications and services by scanning your source code for vulnerabilities and weaknesses.
* **Dynamic Analysis** - Assess your applications and services while they are running by leveraging the [Review App](https://docs.gitlab.com/ee/ci/review_apps/) available as part of GitLab’s CI/CD functionality.
* **Composition Analysis** - Assess your applications and services by analyzing dependencies for vulnerabilities and weaknesses, confirming only approved licenses are in use, and scanning your containers for vulnerabilities and weaknesses.
* **Fuzz Testing** - Assess your applications and services by inputting unexpected, malformed, and/or random data to measure response or stability by monitoring for unexpected behavior or crashes.
* **Threat Insights** - Holistically view, manage, and reduce potential risks across the entire DevSecOps lifecycle including Security Merge Request Views, Pipeline Security Reports, and Security Dashboards at the Project, Group, and Instance level.
* **Container Security** - Monitor and protect your cloud-native applications and services by leveraging context-aware knowledge to improve your overall security posture.
* **Vulnerability Research** - Leverage GitLab research to empower your Secure results by connecting security findings to industry references like [CVE IDs](https://cve.mitre.org).

### Teams and Investments

#### Team members

The existing team members for the Sec Section can be found in the links below:

* [Development](https://about.gitlab.com/company/team/?department=sec-section)
* [User Experience](https://about.gitlab.com/company/team/?department=secure-ux-team)
* [Product Management](https://about.gitlab.com/company/team/?department=sec-pm-team)
* [Quality Engineering](https://about.gitlab.com/company/team/?department=secure-enablement-qe-team)

#### Investments

Learn more about GitLab's investment into the Sec section by visiting our
[Product Investment](https://about.gitlab.com/handbook/product/investment/) page within the
[Product Handbook](https://about.gitlab.com/handbook/product/).

### Accomplishments, News, and Updates

The following are the updates from the Sec Section for April 2021 (as presented in the monthly
[Product Key Review](https://about.gitlab.com/handbook/key-review/)). A complete list of released features
can be found on the [Release Feature Overview](https://gitlab-cs-tools.gitlab.io/what-is-new-since/?selectedStages=secure&selectedStages=protect)
page and a complete list of upcoming features can be found on the [Upcoming Releases](https://about.gitlab.com/upcoming-releases/) page.

#### Section & team member updates

* [We are actively hiring a Director, Product Management role for the Secure and Protect stages](https://gitlab.com/gitlab-com/Product/-/issues/2127)

#### Important PI milestones

* [Continued underreporting due to offline environments impacting Secure Stage SMAU](https://app.periscopedata.com/app/gitlab/445781/ARR-and-Customer-Info?widget=5760020&udv=0)
* [Secure Stage MAU continues to grow MoM with 16% growth in March](https://about.gitlab.com/handbook/product/sec-section-performance-indicators/#sec-secure---section-mau-smau---unique-users-who-have-used-a-secure-scanner)
* [Static Analysis GMAU continues to grow MoM with 20% growth in March](https://about.gitlab.com/handbook/product/sec-section-performance-indicators/#securestatic-analysis---gmau---users-running-static-analysis-jobs)
* [Dynamic Analysis GMAU continues to rebound MoM with second month of growth](https://about.gitlab.com/handbook/product/sec-section-performance-indicators/#securedynamic-analysis---gmau---users-running-dast)

#### Recent accomplishments

* [API fuzz testing graphical configuration screen](https://docs.gitlab.com/ee/user/application_security/api_fuzzing/#configuration-form)
* [API fuzz testing supports OpenAPI v3 files](https://docs.gitlab.com/ee/user/application_security/api_fuzzing/#configuration)
* [Clickable file and line number links on Vulnerability Report](https://docs.gitlab.com/ee/user/application_security/vulnerability_report/#view-vulnerable-source-location)
* [Support Java 15 for Dependency Scanning](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#configuring-specific-analyzers-used-by-dependency-scanning)
* [Vulnerability bulk status updates](https://docs.gitlab.com/ee/user/application_security/vulnerability_report/#change-status-of-vulnerabilities)
* [Add icons to the Vulnerability Trends Chart](https://docs.gitlab.com/ee/user/application_security/security_dashboard/#project-security-dashboard)

#### What’s ahead

* [Configuration UI for Secret Detection](https://gitlab.com/groups/gitlab-org/-/epics/4496)
* [Improve tracking accuracy of SAST, Secret Detection findings](https://gitlab.com/groups/gitlab-org/-/epics/5144)
* [DAST on-demand: Select branch for on-demand scans](https://gitlab.com/groups/gitlab-org/-/epics/4847)
* [Browserker Horizon 2: Authentication improvements](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/issues/25)
* [Consistency in default behaviour of AST scanners and jobs](https://gitlab.com/groups/gitlab-org/-/epics/5334)
* [Vulnerability Dismissal Types / Reasons](https://gitlab.com/groups/gitlab-org/-/epics/4942)
* [Create a security vulnerability from API](https://gitlab.com/gitlab-org/gitlab/-/issues/10272)
* [Create ruby-based container scanning tool based on Trivy](https://gitlab.com/groups/gitlab-org/-/epics/5398)

## 3 Year Section Themes

<%= partial("direction/secure/templates/themes") %>

<%= partial("direction/protect/templates/themes") %>

## 1 Year Plan

### What We Recently Completed

The Secure team has been actively delivering updates to help you reduce risk.  The following are some highlights from recent GitLab releases:
* **Integrations** - Enable third parties to easy integrate their security solutions into Ultimate (self-hosted) / Gold (GitLab.com). This includes providing APIs and a standard reporting framework so everyone can bring their preferred security tools into Ultimate (self-hosted) / Gold (GitLab.com).
* **Responsible disclosure** - GitLab will become both a CVE Numbering Authority (CNA) for GitLab applications as well as for researchers and technologists to use when reporting new vulnerabilities (within any application, service, or operating system).  As part of Secure’s 3 Year Strategy, we want to support the entire vulnerability lifecycle to enable ease-of-use when it comes to [responsible disclosure](https://en.wikipedia.org/wiki/Responsible_disclosure).
* **API first** - API proliferation will continue as more companies focus on ways to integrate technologies.  As such, we will focus on security testing of APIs including verifying standards like the [OWASP Top 10](https://www.owasp.org/index.php/Category:OWASP_Top_Ten_Project) and [API fuzz testing](https://gitlab.com/gitlab-org/gitlab/issues/33906).
* **Provide Dynamic Analysis in production** - Enable Dynamic Analysis Categories to empower users to scan and assess applications and services deployed to production / operations.


### What We Are Currently Working On

The Secure team is actively working to bring world class security to DevSecOps and the following outlines where we are currently investing our efforts:
* **Application Security Testing (AST) Leadership** - We will take a leadership position within the Application Security Testing (AST) market.  This will be accomplished by focusing on moving [Static Analysis (SAST)](https://about.gitlab.com/direction/secure/#sast), [Dynamic Analysis (DAST)](https://about.gitlab.com/direction/secure/#dast), and [Dependency Scanning](https://about.gitlab.com/direction/secure/#dependency-scanning) categories to [Complete](https://about.gitlab.com/direction/maturity/) maturity as well as moving [Vulnerability Management](https://about.gitlab.com/direction/secure/vulnerability_management/) to [Viable](https://about.gitlab.com/direction/maturity/) maturity and introducing a [Fuzz Testing](https://about.gitlab.com/direction/secure/#fuzz-testing) category and bringing it to [Viable](https://about.gitlab.com/direction/maturity/) maturity.
* **Dogfooding** - We will [“practice what we preach”](https://www.dictionary.com/browse/practice-what-you-preach), including leveraging Secure Categories in all things GitLab does.  This tight circle will provide immediate feedback and increase our rate of learning.
* **Security for everyone** - In order to make security accessible to everyone across the DevOps lifecycle, we will bring all Secure OSS scanners to Core (self-hosted) / Free (GitLab.com).
* **Security Orchestration** - Provide a unified user experience for viewing, triaging, and resolving alerts from cloud-native environments while also enabling security policy management across development, staging, and production.


### What's Next For Us

To meet our [audacious goals](https://about.gitlab.com/company/mission/#big-hairy-audacious-goal), the Secure Stage will focus on the following over the next 12 months:
* **Historical trending** - Provide a focus on identifying patterns in security findings with a goal of helping everyone code securely.  Make recommendations on remediation with a goal of providing [automatic remediation](#automatic-remediation) wherever possible.
* **Protocol fuzzing** - Fuzzing the entire application technology stack is part of Secure’s 3 Year Strategy; however, we will focus on applications and APIs first.  The shift to protocol fuzzing will occur as Viable and Complete [maturities](https://about.gitlab.com/direction/maturity/#legend) are achieved on DAST and API security testing.
* **Differentiate on value** - Running a security test is just the beginning.  We want to provide a first-class experience and enable users to make data-driven decisions to secure their applications and services as well as their enterprise.  Our [Security Dashboards](https://docs.gitlab.com/ee/user/application_security/security_dashboard/#gitlab-security-dashboard) and [Merge Request Approvals](https://docs.gitlab.com/ee/user/application_security/index.html#security-approvals-in-merge-requests) is just the beginning.

In addition, the Protect Stage will focus on the following over the next 12 months:
* **Cross-stage Security Policy Management** - Allowing users to configure and manage security policies in a unified manner across their GitLab Workspace.  This includes managing policies for applications running in production (such as managing network policies) as well as managing policies that specify when scans should be required to run.
* **Cross-stage Security Alert Management** - Allowing users to interact with a single dashboard to review and manage their Security Alerts across all of GitLab.  This includes Alerts generated from production environments, Alerts generated from the results of scans, and other GitLab security-related Alerts.
* **Integrated Cloud Workload Protection** - GitLab aims to consolidate and integrate the tools required to adequately protect cloud-based workloads.  This includes allowing for regular scans of containers that are running in production as well as providing the necessary tools to monitor and harden your production environments.


### What We're Not Doing

The following will NOT be a focus over the next 12 months:
* **Machine learning** - Machine learning (ML) will be leveraged, as part of static analysis, to identify insecure coding practices and help developers write more secure code.  This will be opt-in and will enable the power of the GitLab global community.
* **Security services** - The cybersecurity staffing shortage [continues to grow](https://www.forbes.com/sites/martenmickos/2019/06/19/the-cybersecurity-skills-gap-wont-be-solved-in-a-classroom/#79a380dd1c30) with no solvable solution yet defined.  To solve this issue, organizations have been relying on security services to fill this gap in their security processes.  As part of Secure’s 3 Year Strategy, we want to address this for the GitLab community by offering cybersecurity augmentation powered by GitLab Secure categories.
* **SIEM functionality** - Security Information and Event Management (SIEM) solutions are a common tool leveraged by IT Ops and SecOps organizations to monitor for events within their production environments. The Protect stage will support exporting security events to external SIEM solutions, including the Monitor stage, as well as Monitor on-call functionality.
* **Non-cloud native environments** - Enterprises are undergoing cloud-native transformations shifting from traditional data centers to public cloud environments leveraging technologies like Kubernetes. Protect will focus on meeting enterprises where they are going and not focus on where they are coming from.


## Target audience

GitLab identifies who our DevSecOps application is built for utilizing the following categorization. We list our view of who we will support when in priority order.
* 🟩- Targeted with strong support
* 🟨- Targeted but incomplete support
* ⬜️- Not targeted but might find value

### Today
To capitalize on the [opportunities](#opportunities) listed above, the Sec Section has features that make it useful to the following personas today.
1. 🟩 Developers / Development Teams
1. 🟩 Security Teams
1. 🟨 SecOps Teams
1. 🟨 QA engineers / QA Teams
1. ⬜️ Security Consultants

### Medium Term (1-2 years)
As we execute our [3 year strategy](#3-year-strategy), our medium term (1-2 year) goal is to provide a single DevSecOps application that enables collaboration between developers, security teams, SecOps teams, and QA Teams.
1. 🟩 Developers / Development Teams
1. 🟩 Security Teams
1. 🟩 SecOps Teams
1. 🟩 QA engineers / QA Teams
1. 🟨️ Security Consultants

## Stages and Categories

The Sec section is composed of two stages, [Secure](/direction/secure/) and [Protect](/direction/protect/), each of which contains several categories. Each stage has an overall
strategy statement below, aligned to the themes for Sec. Each category within each stage has a dedicated direction page
plus optional documentation, marketing pages, and other materials linked below.

<%= partial("direction/secure/templates/strategies") %>

<%= partial("direction/secure/templates/strategies") %>

## Upcoming Releases

<%= direction["all"]["all"] %>

<p align="center">
    <i><br>
    Last Reviewed: 2020-12-29<br>
    Last Updated: 2020-12-29
    </i>
</p>
