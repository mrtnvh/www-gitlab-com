---
layout: handbook-page-toc
title: "Edit this website locally"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

This is a guide on what you'll need to install and run on your machine so you can make edits locally. This is quicker than in the web interface and allows you a better overview and preview when making complex changes.
Once you're set up, you will find the source files for the GitLab Marketing Website and GitLab Handbook here [/source/handbook/](https://gitlab.com/gitlab-com/www-gitlab-com/tree/master/source)

## 1. Help is available for team members

### Editing the Handbook

If you work for GitLab Inc., we don't expect you to figure this out by yourself.
If you have questions, ask anyone for help or post in the #handbook Slack channel. You're more likely to have success with:

- People that have [Merge Request buddy](/handbook/people-group/general-onboarding/mr-buddies) as an expertise on the [team page](/company/team/).
- People that have been here longer than 3 months. The [team page](/company/team/) lists people in order of how long they have been here.
- People that have 'engineer' in their title.
- We encourage you to meet someone new by asking someone you don't know for help. You can also ask for help in #questions or #peopleops by posting 'Who can do a video call with me to help me work on the website locally /handbook/git-page-update/ ?'. If the other options don't work for you, please ask your onboarding buddy.
- Suggest improvements to the GitLab marketing website with the [help of the growth marketing team](/handbook/marketing/inbound-marketing/#requesting-support) -- (e.g. [requesting a new page](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/new?issuable_template=request-new-website-brief), [updating an existing page](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/new?issuable_template=request-update-website-brief), or [suggesting a promotion on the front page of about.gitlab.com](/handbook/marketing/inbound-marketing/content/#homepage-promotion-guidelines)

## 2. Start using GitLab

1. Here's where you can find step-by-step guides on the [basics of working with Git and GitLab](https://docs.gitlab.com/ee/gitlab-basics/README.html). You'll need those later.
1. Create your [SSH Keys](https://docs.gitlab.com/ee/gitlab-basics/create-your-ssh-keys.html).
1. For more information about using Git and GitLab see [GitLab University](https://docs.gitlab.com/ee/university/).

## 3. Install Git

1. Open a terminal.
1. Check your Git version by executing: `git --version`.
1. If Git is not installed, you should be prompted to install it. Follow this [guide](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html) to installing Git and
linking your account to Git.

## 4. Install Ruby Version Manager (RVM)

[Ruby Version Manager (RVM)](https://rvm.io/) is a command-line utility that allows you to easily install and switch between different versions of the [Ruby](https://www.ruby-lang.org/) programming language, which is used to build this website. It can be installed directly from a command prompt (e.g., in macOS Terminal) with the following command:

```bash
curl -sSL https://get.rvm.io | bash -s stable
```

Once RVM has been installed, ensure that your updated terminal environment has been loaded by closing your terminal window and opening a new one.

## 5. Install Ruby and Bundler

The version of Ruby that you'll need to build this website will change from time to time, but you can always check what version that is by looking at the contents of `.ruby-version` in the root of the `www-gitlab-com` directory. You can always check it [on GitLab](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/.ruby-version) even if you haven't yet cloned your own local copy of the repository.

If `.ruby-version` specifies version `2.6.5`, you can install it with RVM like so:

1. In a terminal, execute: `rvm install 2.6.5` to install Ruby
   (enter your system password if prompted).
1. Execute: `rvm use 2.6.5 --default` to set your default Ruby to `2.6.5`.
1. Execute: `ruby --version` to verify Ruby is installed. You should see:
   `ruby 2.6.5p114 (2019-10-01 revision 67812)`.
1. Execute: `gem install bundler` to install [Bundler](http://bundler.io/).

**Ruby 3 note:**

We upgraded to Ruby 3.0.0 on Monday, 2021-04-26. We put together some [upgrade instructions in the MR comments, which we shared throughout Slack](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/78850#note_559337911). If you're still having issues with the Ruby 3.0.0 upgrade, please reach out via the [follow-up issue](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1273)

## 6. Clone the source of the website and install its dependencies

1. If you set up SSH keys previously, in terminal execute: `git clone git@gitlab.com:gitlab-com/www-gitlab-com.git` to clone the website. If you prefer using https, then execute: `git clone https://gitlab.com/gitlab-com/www-gitlab-com.git`, but note that if you've activated 2FA on your GitLab.com account, you'll need to take some additional steps to set up [personal access tokens](https://gitlab.com/profile/personal_access_tokens). If you ever want to switch between SSH and https, execute `git remote remove origin`, followed by `git remote add origin [..]` where the `[..]` is the part that starts with `git@` for SSH, or with `https:` for https.
1. Execute: `cd www-gitlab-com` to change to the `www-gitlab-com` directory.
1. Execute: `bundle install` to install all gem dependencies.
1. Install additional dependencies [Homebrew, nvm, yarn, and rbenv](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/doc/development.md).  If not installed, step 8 may result in an error.

## 7. Prevent newlines from causing all following lines in a file to be tagged as changed

Git is able to automatically convert line endings between `CRLF` and `LF` and vice versa. Execute the following command to configure Git to convert `CRLF` to `LF` on commit but otherwise leave line endings alone:

```bash
git config --global core.autocrlf input
```

If you'd like to learn more about Git's formatting and whitespace options see the "Formatting and Whitespace" section under "[Git Configuration](https://git-scm.com/book/en/v2/Customizing-Git-Git-Configuration)" in "[Pro Git](https://git-scm.com/book/)".

## 8. Preview website changes locally

1. In a terminal, execute:
    1. Change to the directory for your site, for example: `cd sites/handbook` (see [monorepo docs](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/doc/monorepo.md) for more details)
    1. `NO_CONTRACTS=true bundle exec middleman`.
1. Wait for the log line: `== View your site at "http://localhost:4567", "http://127.0.0.1:4567"`.
1. Visit http://localhost:4567/ in your browser.
1. You will need to install a text editor to edit the site locally. We recommend
   [Sublime Text 3](http://www.sublimetext.com/3) or [Atom](https://atom.io/). Use command / ctrl P to quickly open the file you need.
1. Refresh your browser to see your changes.

## 9. Test if all URL links in a page are valid

Until this is automated in CI, a quick way to see if there are any invalid
links inside a page is the following.

1. Install the [check-my-links](https://chrome.google.com/webstore/detail/check-my-links/ojkcdipcgfaekbeaelaapakgnjflfglf/) extension in Chrome or the [Broken Link Checker](https://addons.mozilla.org/en-US/firefox/addon/find-broken-links/) addon in Firefox.
1. Open the page you wish to preview (see previous step).
1. Click the newly installed extension in the upper right corner of Chrome.

A pop-up window will open and tell you how many links, if any, are invalid.
Fix any invalid links and ideally any warnings, commit and push your changes,
and test again.

All internal links (links leading to other parts of the website) should be relative.

## 10. Start contributing

Most pages that you might want to edit are written in markdown [Kramdown](http://kramdown.gettalong.org/).
Read through our [Markdown Guide](/handbook/markdown-guide/) to understand its syntax and create new content.

Instructions on how to update the website are in the
[readme of www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/README.md).

## 11. Add yourself to the Team Page

We are happy to have you join our company and to include you in our [team page](/company/team/)! [A sync](/handbook/people-group/engineering/miscellaneous/#sync-to-team-page) will add a basic entry for you on our team page on your second day of employment at GitLab. You are invited to personalize this entry and add more information to it.

Ask anyone in the company for help if you need it - you can use either the #mr-buddies or #questions Slack channel for this purpose. There are **three** ways to update the website.  Choose the method below that feels most comfortable and have the following information handy:

* An invitation to the [www-gitLab-com project](https://gitlab.com/gitlab-com/www-gitlab-com) at your GitLab email account.
* Name of the People Experience Associate helping you with onboarding.
<a name="picture-requirements"></a>
* A picture of yourself for the team page
  > **Picture Requirements**
  >
  > * Crop image to a perfect square.
  > * Keep maximum dimension under 400 by 400 pixels.
  > * Use the JPEG (`.jpg`) or PNG (`.png`) format.
  > * Keep the file size below 100k.
  > * Test image in color and black-and-white (you will add the color version).
  > * The image file should be located in the folder `sites/marketing/source/images/team/`
  > * Name file `yournameinlowercase` and add the appropriate file extension.
* Story about your background and interests. (See other team member profiles for examples.)
* Your personal LinkedIn / Twitter / GitLab handles
* A relative link to your role. If your link is `https://about.gitlab.com/job-families/engineering/support-engineer/` use `/job-families/engineering/support-engineer/`. Refer to other entries for reference.

### Method 1: Add your info on GitLab.com using the Web IDE

<figure class="video_container"><iframe src="https://www.youtube.com/embed/J0uZYxGGerE" width="560" height="315"></iframe></figure>

  1. Go to the [Team page](https://about.gitlab.com/company/team/) and find yourself. Note: if you choose `No` on BambooHR for `Export Name/Location to Team Page?` you will have to find yourself by your job title instead of your name. 
  1. Click on the avatar above your name (or job title). A modal will open.
  1. In that modal, on the bottom, click `Edit in Web IDE`
  1. Our web editor will open with your team page entry opened.
  1. <%= partial "team-page-fields" %>
  1. Once you have finished this, click the `Commit` button in the bottom left. It should say something like `2 unstaged and 0 staged changes`. This will bring up a sidebar with an `Unstaged` and `Staged` area.
  1. Check the files to ensure your updates are what you expect. If they are, click the check mark above the changed file list to "stage" all changes. You can also use the `Stage` button in the upper-right of the changed file view to stage each one individually. You should now see `0 unstaged and 2 staged changes`.
  1. Once you have verified all of the edits, click commit and enter a short commit message in the box that appears. Your commit message should describe what you have changed. Choose `Create a new branch`. Name the branch in the format of `yourinitials-add-YOURNAME-to-team-page-date` or similar. Example: `plh-add-paulalilyherbert-to-team-page-feb06`
  1. Tick the `Start a new merge request` checkbox. Then click `Commit` once more.
  1. Fill out the merge request details and assign it to your manager. If your manager does not have access to merge your MR based on the permissions described above, then please reference the specific members page for the www-gitlab-com project for review.  Check the `Delete source branch when merge request is accepted` box to cleanup your merge request when complete. Click `Submit merge request` to submit the MR for review.


### Method 2: Add your info on GitLab.com using the 'web interface'

  1. Go to the [GitLab.com / www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com/) project.
  1. Click the `+` under the red line near the top of the screen.
  1. Click `New branch`.
  1. For `Branch name`, name it something unique (it's temporary so don't worry too much about the exact name) like *your initials-team-page-update-yourdepartment-the date* and click `Create branch`. Example: `hk-team-page-update-custsupport-feb06`
  1. Start by adding your image. Click on `Repository` on the left side then `Files`.
  1. In the file browser, navigate to `sites/marketing/source/images/team`
  1. At the top of the page click `+` and choose `Upload file` to upload your picture. Be sure to follow the [picture requirements](#picture-requirements). Add text *Add YourFirstName YourLastName to team page* and click `Upload file`.
  1. Navigate on your branch near the top of the page following the text that has your unique branch name and click on the text that follows your branch name `www-gitlab-com`.
  1. Now you will edit your biographical information. All the bio information displayed on the Team page is pulled from a data file. Click on `data`, and then scroll down to `team_members/person/FIRST_LETTER_OF_YOUR_FIRST_NAME/SLUG_REPLACE.yml` (you are looking for a file that specifies your name or slug).
  1. Click on `edit` on the top right side of your screen.
  1. <%= partial "team-page-fields" %>
  1. After you added your information, add a comment to your commit and click on “Commit Changes”.
  1. Now [Create a merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html) in [GitLab.com](https://gitlab.com/gitlab-com/www-gitlab-com) with the branch that you created by clicking `Create merge request` button.
  - Create a title that describes your changes at a high level.
  - Add a description of your changes
  - Assign the merge request to yourself
  - Make sure the source branch is the one you created `hk-team-page-update-custsupport-feb06` (as an example from above) and the target is `master`
  - Check the box `delete source branch when merge request is accepted`
  1. Click `submit merge request`
    At the upper right of the new page, click `edit` next to `Assignee` and also assign the merge request to your manager.

### Method 3: Add your info using a Local Git clone (using the terminal and an IDE)
  *Note:* This method may take longer than other methods, because it requires `git clone` for around 4GB size repository.

  1. Download Git, following the [start using git documentation](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html).
  1. Follow the steps to create and add your [SSH keys](https://docs.gitlab.com/ee/gitlab-basics/create-your-ssh-keys.html).
  1. Clone the [www-gitlab-com project](https://gitlab.com/gitlab-com/www-gitlab-com) through your shell, following the [command line commands documentation](https://docs.gitlab.com/ee/gitlab-basics/command-line-commands.html).
  1. Create and checkout a new branch for the changes you will be making.
  1. Add your picture to the `sites/marketing/source/images/team/` directory in the repository and `git add` it. Be sure to follow the [picture requirements](#picture-requirements).
  1. Open `data/team_members/person/FIRST_LETTER_OF_YOUR_FIRST_NAME/SLUG_REPLACE.yml` in your favorite editor, specifically looking for the file with your name or slug.
  1. <%= partial "team-page-fields" %>
  1. Save the changes to the file in `data/team_members/person/FIRST_LETTER_OF_YOUR_FIRST_NAME/` that you just edited, and `git add` it.
  1. To see your changes locally:
        1. Manually run a command to compile the changes you just made into a file that actually populates the team page:<br/>
           ```
           cd <WWW-GITLAB-COM REPO ROOT>
           bundle exec rake build:team_yml
           ```
        1. Follow the directions in `README.md`.
  1. After validating your changes, commit your changes to the branch of www-gitlab-com that you created in step 4, with a comment *Add FirstName LastName to team page* and push your branch. You may need to set the remote as upstream or you can use `--set-upstream` option and specify remote as upstream.
  1. [Create a Merge Request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html) in [GitLab.com](https://gitlab.com/gitlab-com/www-gitlab-com) with the branch that you created and assign it to your manager for review.

  *Note:* When you test locally, the map on top of the page won't show your photo. This is because it is not populated with local data. [More about how the map works](https://gitlab.com/gitlab-com/teampage-map/blob/master/README.md#background). You will see your picture on the map as soon as your Merge Request is merged.
  *Note:* Searching the handbook in your local environment yields production results, so navigate directly to the team page via URL to see your changes.

### Add your pet(s) to the Team Pets Page

  Using what you learned in the [steps above](#11-add-yourself-to-the-team-page), consider adding your pet to the [Team Pets page](/company/team-pets/). You can follow these instructions for adding them via the Web IDE.

  1. Again, find the picture that you'd like to add to the team pets page, and update the picture's name to the following format: `petname.jpg` or `petname.png`. Ensure the picture size is around 400x400 (*it must be square*, see [picture requirements](#picture-requirements)).
  1. Go to the [GitLab.com / www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com/) project.
  1. On the Repository page, you will see a Web IDE button near the middle of the page next to History and Find File buttons.
  1. In the file browser, navigate to `sites/marketing/source/images/team/pets`.
  1. Click the `⋁` icon next to the `pets` directory and choose upload file and choose the photo you prepared in step 1.
  1. Next, navigate to in the file browser `data/pets.yml` and click on it to open the editor.
  1. Add your pet by following the format of the existing pets on the page (you can copy and paste their lines of code, even). Ensure that you include your pet's name, your full name, and the image you uploaded in step 1.
  1. Once you have finished this, click the `Commit` button in the bottom left. It should say something like `2 unstaged and 0 staged changes`. This will bring up a sidebar with an `Unstaged` and `Staged` area.
  1. Check the file to ensure your updates are what you expect. If they are, click the check mark next to the filename to "stage" these changes.
  1. Once you have verified all of the edits, click commit once more. Here, enter a short commit message including what you've changed. Choose `Create a new branch and merge request`. Choose a branch name of your choosing. Remember to name it something unique. We recommend the format *your initials-team-page-update-yourdepartment-the date* Example: `mh-team-page-update-custsupport-feb06`
  1. Fill out the merge request details and assign it to your manager for review.

## 12. Edit the Handbook

### WebIDE, using the browser
The Web Integrated Development Environment (IDE) is used to make changes within the browser. This method requires no setup.

1. Find the handbook page to edit.
1. Click on the `Edit this page` button at the bottom of the page.
1. On the page, you should see the relevant file in the repository displayed. Click on the `Web IDE` button on the right side.
1. Edit the page using [MarkDown](/handbook/markdown-guide/). You can preview your changes (but links will not work).
    * Note: You can edit other pages by browsing through the filelist on the left side in the Web IDE.
1. After making your changes, click the `Commit...` button in the bottom left to review the list of changed files. Click on each file to review the changes and click the tick icon to stage the file.
1. Once you have staged some changes, you can add a commit message and commit the staged changes. The message should be as brief as possible, since it has a character limit. You can add more detail in the description in a subsequent step. Unstaged changes will not be committed.
1. Choose the option to create a new branch and create merge request. For your branch, use a descriptive name with hyphens and no spaces, so that you can find it again later, but it's temporary so don't worry too much about the exact name. We recommend the format *your initials-team-page-update-yourdepartment-the date* Example: `mh-team-page-update-custsupport-feb06`.
1. Submit and you will be taken to the merge request (MR) page.
1. Feel free to add a more detailed message in the description box. For the options, check `Remove source branch`.
1. Assign the MR to the [Directly Responsible Individual (DRI)](/handbook/people-group/directly-responsible-individuals/):
  - If the DRI for the page(s) being updated isn't immediately clear, then assign it to your manager.
  - If your manager does not have merge rights, please ask someone to merge it _after_ it has been approved by your manager in [#mr-buddies](https://gitlab.slack.com/archives/CLM8K5LF4).

### Locally, using the terminal

  1. If you haven't already, follow steps 1-5 in the "Add yourself to the Team Page"'s "Add Locally (using the terminal)" section above.  (This step is necessary as the handbook lives in the same repository as the rest of GitLab.com).  If you're following this guide in order and have already added yourself to the team page, instead go back to the main branch (via `git checkout master`) and there create a new branch for your handbook edits.
  2. The handbook lives under `sites/handbook/source/handbook`.  For the most part, you can locate the specific item to edit via that item's URL.  For instance, this page is /handbook/git-page-update/ and its source lives in `sites/handbook/source/handbook/git-page-update/index.html.md.erb`.
  3. Edit away!  See the "Start Contributing" section, above, for details about the Markdown that most pages are written in.
  4. Preview your changes locally by following the directions in `README.md`.  Keep in mind that the local server won't auto-reload when you change a page, so you'll need to restart it to see what you've done.
  5. Once you've made your changes and verified they appear the way you want them to, commit them with a comment and push your branch.
  6. As above, [Create a Merge Request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html) in the [www-gitlab-com project](https://gitlab.com/gitlab-com/www-gitlab-com).  If you're onboarding, don't forget to assign it to your manager.

## Marking a merge request as a Work in Progress (WIP)

1. You can easily prevent a merge request from being merged before you're ready by marking it as a *work in progress*. Simply type "WIP:" at the beginning of your merge request title (e.g. "WIP: My Handbook Change"). To merge once you're ready, edit your MR and remove "WIP:" from the title.
  1. **Note:** Only mark a merge request as "WIP" (Work in Progress) if it will negatively affect the company if merged too early. That can be the case for application code but is almost never possible for handbook MRs.

## Ready to merge

Handbook Merge Requests should have the branch set to delete.
It should not have commits set to squash.
We do not squash commits because of the [possible negative effects](https://medium.com/@fredrikmorken/why-you-should-stop-using-git-rebase-5552bee4fed1).
Then assign the MR to a maintainer for review and merging.

## Where should content go?
GitLab has a lot of places you can put web content including the [website](/handbook/marketing/inbound-marketing/digital-experience/), [blog](/handbook/marketing/blog/), [docs](https://docs.gitlab.com/ee/development/documentation/index.html), and the [handbook](/handbook/handbook-usage/). Here's an overview of where you should create a merge request to add content.
1. **[blog](/handbook/marketing/blog/)**: The blog is a great place to start. If you don't know where to put content, then write a blog post! Great blogs can always be then copied or modified for the website, docs, and handbook later. Blog posts are especially good for news, announcements, and current trends because blog posts are tied to a specific date.
1. **[website](/handbook/marketing/inbound-marketing/digital-experience/)**: This is the main marketing site for GitLab and where folks will tend to go first to find out information about GitLab (the product and the company). The website contains a broad set of content from [product pages](/product) to [customer case studies](/customers). The website is the best place for [evergreen](https://www.wordstream.com/blog/ws/2012/10/16/guide-to-evergreen-content-marketing) articles such as [topic](/handbook/marketing/inbound-marketing/digital-experience/#topics) and [solution](/handbook/marketing/inbound-marketing/digital-experience/website/#solutions) pages since it's not tied to specific date.
1. **[docs](https://docs.gitlab.com/ee/development/documentation/index.html)**: The docs are where are all technical information about GitLab self-managed and GitLab.com belongs. If the intended audience for the content is a user of GitLab then it should be in the docs. The docs are great place for both reference docs (what are the configurable settings for a feature, e.g. [what can you do with issues](https://docs.gitlab.com/ee/user/project/issues/index.html)) and narrative docs (how to do x with y, e.g. how to set up [HA on AWS](https://docs.gitlab.com/ee/install/aws/index.html#doc-nav)).
1. **[handbook](/handbook/handbook-usage/)**: The handbook is for any content that needs to be [used by GitLab team-members to do their job](/handbook/handbook-usage/). If you put content on the blog, website, and docs, but you think it would be helpful for GitLab team-members to do their job then link to the content from the handbook.

Sometimes the lines are blurry and it can seem as though there are multiple places that would be a good fit. For example, "how to" articles make great blog posts, but could be more discoverable to users if they are in the docs. Just pick one. It's better to create content somewhere than nowhere. When in doubt, start with the blog.
