---
layout: handbook-page-toc
title: "Key Reviews"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Purpose

For each [executive](/company/team/structure/#executives) — and some department leaders — to discuss the key metrics of that department in order to:

1. Make it much easier to stay up-to-date for everyone.
1. Be accountable to the rest of the company.
1. Understand month-to-month variances.
1. Understand performance against the plan, forecast and operating model.
1. Ensure there is tight connection between OKRs and KPIs.
1. Ensure that progress against OKRs is understood.

## Key Metrics

1. [KPIs](/company/kpis/) of that function or department.
1. [OKRs](/company/okrs/) for that function.

## Timing
Meetings are scheduled monthly starting on the 15th day after month end.  

## Scheduling
The EBA to the CFO is the scheduling DRI for the Monthly Key Reviews, with the exception of the Sales Key Review, the EBA to the CRO is the scheduling DRI for the Sales Key Review.

A Key Review should not be cancelled, permission to cancel or make changes to the Key Review schedule must be approved by the CFO. If the CFO is not available the CEO will make the decision. Requests should be posted in the [#key-meeting](https://gitlab.slack.com/archives/CPQT0TRFX) Slack channel. All changes to the Key Review schedule and/or invites have to be posted in the [#key-meeting](https://gitlab.slack.com/archives/CPQT0TRFX) Slack channel.

Each month's Key Review schedule will be posted in the [#key-meeting](https://gitlab.slack.com/archives/CPQT0TRFX) Slack channel on the first Monday of each month (if that Monday falls on a holiday it will be posted on the next business day).

## Invitees

Required invites are the CEO, the CFO, and the function head.
Optional attendees are all other members of [the e-group](/company/team/structure/#e-group).
The meetings are open to all team members who are interested.
The meetings are also to be livestreamed to GitLab Unfiltered.

Functions that have these meetings are:

*  Development (Christopher Lefelhocz - Function DRI) scheduled every other month
*  Quality (Mek Stittri - Function DRI) scheduled every other month
*  UX (Christie Lenneville - Function DRI) scheduled every other month
*  Security (Johnathan Hunt - Function DRI) scheduled every other month
*  Finance (Brian Robins - function DRI) scheduled monthly
*  Infrastructure (Steve Loyd - function DRI) scheduled monthly
*  Legal (Robin Schulman - function DRI) scheduled monthly
*  Marketing (Todd Barr - function DRI) scheduled monthly
*  People Group (Wendy Barnes - function DRI) scheduled monthly
*  Product (Scott Williamson - function DRI) scheduled monthly
*  Sales (Michael McBride - function DRI) scheduled monthly
*  Support (Tom Cooney - function DRI) scheduled monthly

If you would like to be added to a function's Key Review post in [#key-meetings](https://gitlab.slack.com/archives/CPQT0TRFX) on Slack.

### Key Rotation

We expect that All-Directs have a general understanding of the business beyond their function. They are GitLab leaders who often have to work cross-functionally and need business context to guide their teams. Key Reviews are a forum for understanding what is happening in other functions. The Key Rotation is designed to encourage All-Directs to make time to learn about what is happening in other functions. 

Each month, one All-Direct will join the Key Rotation. While no meeting is mandatory, the person should plan to attend all Key Reviews in that month. The team member is encouraged to contribute questions and comments to the agenda in each of these meetings. The team member is also encouraged to bring takeaways back to the team member's E-Group manager and teams.

A rotation includes 10 meetings--each 25 minutes long. The meetings tend to be spread over two weeks. A participant may want to spend some time in advance prepping for these meetings. If a participant has to skip a couple of meetings due to last minute conflicts, the participant can opt to attend the missed sessions in the following month.

Participants can sign up in the Key Rotation Schedule by making an MR to this page and assigning to the [EBA of the CFO](/handbook/eba/#executive-business-administrator-team) in the #key-meetings Slack channel. The EBA to the CFO will add participants to meetings.

#### Key Rotation Schedule

| Month | All-Direct |
| ------ | ------ |
| 2021-02 | [Christie Lenneville - VP UX](https://gitlab.com/clenneville) |
| 2021-03 | [Michelle Hodges - VP WW Channels](https://about.gitlab.com/company/team/#mwhodges) |
| 2021-04 | [Steve Loyd - VP Infrastructure](https://gitlab.com/sloyd) |
| 2021-05 | [Danielle Morrill - Sr. Director Inbound Marketing](https://about.gitlab.com/company/team/#dmor) |
| 2021-06 | [Mek Stittri - Director of Quality](https://gitlab.com/meks) |
| 2021-07 | [David Sakamoto - VP Customer Success](https://about.gitlab.com/company/team/#dsakamoto)  |
| 2021-08 | AVAILABLE |
| 2021-09 | AVAILABLE |
| 2021-10 | AVAILABLE |
| 2021-11 | AVAILABLE |
| 2021-12 | AVAILABLE |

## Meeting Format

There are three meeting formats.
The preferred meeting format leverages the [KPI Slides project](https://gitlab.com/gitlab-com/kpi-slides) which uses Reveal JS to automate the slide preparation and leverages version control to provide a history of changes over time.
Other teams leverage Google Slides for their meetings.
Some teams leverage Sisense's existing automation functionality to prepare Google Slides with automated charts. 

Important notes:
1. Before every Key Review, the OKRs should be updated by the functional DRI by updating the Health Status of their KR issues as detailed in [Maintaining the status of OKRs](https://about.gitlab.com/company/okrs/#maintaining-the-status-of-okrs)
1. A document will be linked from the calendar invite for participants to log questions or comments for discussion and to any additional track decisions & action items.
1. Every department KPI should be covered. This can be in the presentation and/or links to the handbook.
1. Wherever possible, the KPI or KR being reviewed should be compared to Plan, Target, or industry benchmark.
1. There is no presentation; the meeting is purely Q&A. Of course, people can ask to talk them through a slide. If you want to present, please [post a YouTube video](/handbook/marketing/marketing-operations/youtube/) like [Todd did](https://www.youtube.com/watch?v=hpyR39y_1d0) and link that from the slide deck, agenda, and/or Slack.
1. The functional owner is responsible for updating and posting their Key Review agenda 24 hours in advance of the meeting, post in the [#key-meetings](https://gitlab.slack.com/archives/CPQT0TRFX) Slack channel. 
1. Video is not required, but if there is a video it should be posted 24hrs in advance in the [#key-meetings](https://gitlab.slack.com/archives/CPQT0TRFX) Slack channel.
1. The agenda document should include a presentation link that is accessible by all team members, as well as a Q&A section.
1. Key Reviews should be publicly streamed to YouTube Unfiltered in alignment with GitLab's [value of transparency](/handbook/values/#transparency). Key Reviews with [not public](/handbook/communication/#not-public) information should be privately streamed to YouTube Unfiltered. 
1. Should public or private streaming be unavailable, Key Reviews are set to auto-record to the cloud and should be uploaded to the GitLab Unfiltered [Key Reviews Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KrlBfo0IBmV0BwJcPN59swi) on YouTube. It is the DRI's or their EBA's responsibility to ensure that the video is uploaded to GitLab Unfiltered with the correct visibility (private or public). 
1. If the function DRI is not available to attend their Key Review then the function's leadership team is responsible for attending the meeting and providing updates.

Also see [First Post is a badge of
honor](/handbook/communication/#first-post-is-a-badge-of-honor).

### Group Conversations and Key Review Metrics
Function DRIs are expected to use their function's Key Review deck and recordings for their scheduled [Group Conversations](/handbook/group-conversations/). Group Conversations should follow Key Reviews, please see the [Group Conversation schedule](/handbook/group-conversations/#schedule--dri). The goal for these meetings are to discuss what is important and the decks should be the same. You can add additional slides to the Key Review to give more context, not all slides have to relate to KPIs and OKRs. The difference between the two meetings is the audience, the GCs are for a wider audience of the entire company.

### Recordings

To view a recording of a Key Review, visit the playlist on YouTube Unfiltered titled [Key Reviews](https://www.youtube.com/playlist?list=PL05JrBw4t0KrlBfo0IBmV0BwJcPN59swi). If you are unable to view a video, please ensure you are logged in as [GitLab Unfiltered](/handbook/marketing/marketing-operations/youtube/#unable-to-view-a-video-on-youtube). 

Key Review recording links should be shared in the #key-meetings Slack channel by the functional department EBA within 24 hours of a Key Review being completed.

### Automated KPI Slides

The [original demo and proposal of using the KPI Slide project](https://youtu.be/5BlFNhSfS8A) is on GitLab Unfiltered (internal).

The [KPI Slides project](https://gitlab.com/gitlab-com/kpi-slides) is a GitLab-internal project since it includes KPIs that are [not public](/handbook/communication/#not-public).
Those slides are deployed to GitLab pages (also internal, you must have access to the project to see the website).
Each group that presents has one markdown file with their KPIs.
Every month, groups create an MR to update that markdown file.
The following slides need to be updated with an MR:
* Month on the first slide
* Key Business Takeaways- should especially highlight any KPIs that need attention
* OKR statuses

The following Key Reviews are automated: (all links are internal)
* [CEO](https://gitlab-com.gitlab.io/kpi-slides/slides-html/ceo/#/)
* [Finance](https://gitlab-com.gitlab.io/kpi-slides/slides-html/finance/#/)
* [Infrastructure](https://gitlab-com.gitlab.io/kpi-slides/slides-html/infrastructure/#/)
* [Marketing](https://gitlab-com.gitlab.io/kpi-slides/slides-html/marketing/#/)
* [People](https://gitlab-com.gitlab.io/kpi-slides/slides-html/people/#/)
* [Product](https://gitlab-com.gitlab.io/kpi-slides/slides-html/product/#/)

### Google Slides

1. The functional owner will prepare a google slide presentation with the content to be reviewed.
1. The finance business partner assigned to the functional area will meet with the owner at least one week in advance and ensure that follow-ups from last meeting have been completed and that data to be presented has proper definitions and is derived from a Single Source of Truth.
1. The finance business partner / preparer of the key review deck will ensure that the google slide permissions are set to comment for all in the GitLab domain.
1. The title of every slide should be the key takeaway
1. A label on the slide should convey whether the metric result is "on-track" (green), "needs improvement" (yellow), or is an "urgent concern" (red).
1. A [blank template](https://docs.google.com/presentation/d/1lfQMEdSDc_jhZOdQ-TyoL6YQNg5Wo7s3F3m8Zi9NczI/edit) still needs labels.

### Leveraging Sisense in Google Slides
To create these slides, you will need *Editor* access in Sisense. 
* Edit the chart you're interested in. 
* Click "Chart Format" (second tab on far right)
* Under Advanced, select "Expose Public CSV URL"
* Follow the [instructions on pulling data out of Sisense](/handbook/business-ops/data-team/platform/periscope/#pulling-data-out-of-sisense)
* Select the data set and add a chart to the sheet
* In the slides, Insert > Chart > From Sheets > find the one you're looking for

[Video with explanation of this meeting format](https://www.youtube.com/watch?v=plwfGXR9pjw&feature=youtu.be) (GitLab Internal)

### Performance Indicator Pages

Many functions or departments now have Performance Indicator pages that allow one to move top-to-bottom in the handbook to see both KPIs and PIs. 
Here is an example of the VP of Development presenting the [Development Department's KPIs and PIs](/handbook/engineering/development/performance-indicators/) in advance of their monthly Key Review. 

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/8C9oniGinYM" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

This method is ideal, as it is [handbook-first](/handbook/handbook-usage/#why-handbook-first) and makes it easy for [everyone to contribute](/company/strategy/). 
Commentary can be added via an MR to the [data/performance_indicators.yml](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/performance_indicators.yml) file. 
The executive summary section can help consumers of the information understand where to dig in futher. 

The difficulty in using performance indicator pages for Key Reviews is for groups who have a signficant number of Performance Indicators that [cannot be public](/handbook/communication/#not-public). 
For folks looking to consume this information quickly, having to 2FA into Sisense to see the information can be frustrating. 
For functions or departments for which this is true, it is recommended to use a different Key Review format. 

### OKR Slides

OKR slides should: 
- Recap top department OKRs and KRs
- Link to the department epics
- Share completion status and provide a health score
- Flag key achievements
- Highlight risks or dependencies in need of discussion

At the end of each meeting, all atendees should be able to clearly answer what we are trying to achieve and whether we are on track. The Key Review immediately following the close of quarter should address not only new OKRs but also include an update on [scoring](https://about.gitlab.com/company/okrs/#scoring-okrs) of what we have achieved in the previous quarter.
