---
layout: handbook-page-toc
title: "Candidate Experience Specialist Responsibilities"
description: "This page is an overview of the processes, and systems that the Candidate Experience Team is responsible for."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Candidate Experience Specialist Responsibilities
{: #framework-coord}

The CES team operates off of a queue style system with differing levels of priorities based on the request.  The CES team utilizes [GitLab Service Desk](/product/service-desk/) to track all incoming requests to the CES team.


### Best Practices

- The CES team focuses their attention on incoming requests in the following order: CES Service Desk, individual emails, `@ces` slack pings, and lastly slack direct messages.
- You can find the priority list based on the type of request below:
   - 0 - Contracts
   - 1 - Reference & Background checks
   - 2 - Candidate emails
   - 3 - Reschedules
   - 4 - Schedule Interviews
- The CES team now utilizes a new program called [Guide](https://about.gitlab.com/handbook/hiring/talent-acquisition-framework/coordinator/#using-the-candidate-guide) to enhance the candidate experience and create a more transparent view into the talent acquisition processes. One of the features of the Guide is that each Greenhouse stage has a corresponding template that is automatically updated meaning that if a candidate is moved to References or Offer they will see that they are in that stage. Due to this feature, the team needs to be very conscious of when they are moving candidates inbetween stages.

#### How the CES Team will run the Service Desk

1. Under this [CES Service Desk Project](https://gitlab.com/gl-talent-acquisition/ces-service-desk) set up the proper notifications
   - Click on the bell icon on the top right next to Star and Clone
   - Go to Custom Settings
   - Check "New issue"
   - Closeout the window
1. On the left-side menu bar click Issues
   - This is where all our incoming CES emails will create an issue. You'll get an alert when someone sends an email to the CES email alias. Any "emails" that need to be addressed will be an open issue listed within this project.
1. Each CES will begin their workday by triaging issues within the Service Desk based on the [identified priority list](https://about.gitlab.com/handbook/hiring/talent-acquisition-framework/coordinator/#best-practices) by adding the appropriate label to the issue and then will begin working on Level 0 priority requests and so on.
   1. Click on the new Issue
   1. Assign it to yourself on the right-side toolbar
   1. Read the Issue message
   1. Respond to the "email" by adding comments to the issue (be sure to enter comments as you would an email to the candidate)
   1. If this one comment address the entire message and the Application Tracking System (ATS) is the tool needed, add a comment and close the issue

### Req Creation

- The CES team needs to be notified once a req is approved for recruiting
- The Recruiter will open a req kick off issue template
   - A kick off isssue is required for all roles which includes evergreen positions
   - The kick off issue will be the SSOT for documentation regarding all CES tasks required to open a new req
- Internal only roles need to have "Current Team Members Only" on the req name
   - CES will then know to not add the role to the static site and the rest of the conpany will know that we do not accept referrals for it
- Every kick off call will have the entire CES team invited and the team will have a rotation on which specialist will attend

### Screening

- Once a candidate is moved into the Screening stage, the candidate is automatically sent the GSIS.
- When scheduling a candidate for a screening call with a recruiter, the calendly link is to be used. We will no longer use the "Request Availability" option for screening calls as this creates unnecessary manual work
   - If the candidate is sourced, the sourcer should send the calendly
   - If it's an agency candidate or a referral where the recruiter may see the candidate first, the recruiter should send their calendly link

### Team Interviews

- When a candidate is ready to team interview interviews scheduled, the recruiter will ping `@ces` in the Greenhouse profile with next steps
   - If there are shorter timelines for roles as defined by the hiring team, CES needs to be made aware of those timelines for when interviews need to be scheduled. We will default to scheduling interviews at a minimum of [48 hours](https://about.gitlab.com/handbook/hiring/interviewing/#moving-candidates-through-the-process) out to leave time for interviewer prep.
   - Reminder: When tagging `@ces` in Greenhouse, do not tag anyone other than the team in the same ping as it will add those users to the service desk project.
- The CES will request availability via Greenhouse
   - CES will set a reminder to follow up regarding availability if not received within the next 48 hours and will do so via gmail as Greenhouse emails end up in spam sometimes
   - CES will then set another reminder to follow up 24 hours after the initial 48 hour email and will do so via gmail as Greenhouse emails end up in spam sometimes
   - After the 3rd email to candidate with no response, the CES will make the Recruiter aware of the no response and no longer attempt to receive availability
   - Note: Greenhouse reminders should be set for "Coordinator" not a specific person
- After availability is received, CES will schedule interviews based on the interview plans in Greenhouse
   - CES will be using the Zoom integration with [Interview Schedule](https://about.gitlab.com/handbook/hiring/interview-schedule/) for interviews
- If the candidate is an internal candidate, the CES will tag Talent Acquisition Ops and Insights in the GH profile to merge while candidate is still in the Team Interview stage.
- CES will [send interview confirmations via Guide](https://about.gitlab.com/handbook/hiring/talent-acquisition-framework/coordinator/#using-the-candidate-guide)
- CES will not automatically schedule next interviews based off of scorecards
   - The Recruiter or Hiring Manager will need to tag `@ces` in Greenhouse with communication about specific next steps
   - Any candidates who receive a no or definitely not scorecard should be discussed with the Recruiter and the Hiring Manager before declining.

#### Executive Interview Scheduling

For Directors and above with Executive Business Admins:

   1. Prior to candidates moving to the Executive interview stage the Candidate Experience Specialist (CES) ensures the candidate's profile contains a resume/cv,  contact phone number and feedback from prior interviews.
   1. The CES then moves the candidate to Executive Stage in Greenhouse (if applicable)
   1. The CES requests availability from the candidate.
   1. Once the availability is back from the candidate the CES will ping [the appropriate Executive Business Admin (EBA)](/handbook/eba/#executive-business-administrator-team) in the Greenhouse notes section and will include any relevant details such as internal, timezone or high priority.
   1. The EBA will schedule the interview using a unique zoom link, generated by the EBA, and will send out the interview confirmation email to the candidate with a cc to the candidate's CES.
       * For the CTO, all interviews should be scheduled for 60 minutes, the EBA will not use "speedy meetings" for CTO's interviews.
   1. If the EBA is unable to find a suitable time within the candidate's given availability the EBA should suggest times that will work, see the next section for details.

For the VP of Alliances, VP of Commercial Sales, VP of Customer Success, VP of Enterprise Sales, VP of Field Operations and the VP of Worldwide Channels:

   1. Prior to candidates moving to the Executive interview stage the Candidate Experience Specialist (CES) ensures the candidate's profile contains a resume/cv,  contact phone number and feedback from prior interviews.
   1. The CES then moves the candidate to Executive Stage in Greenhouse (if applicable).
   1. The CES pings [the appropriate Executive Business Admin (EBA)](/handbook/eba/#executive-business-administrator-team) in the Greenhouse notes section requesting interview options and will include any relevant details such as internal, timezone or high priority.
   1. The EBA will @ mention the CES in the Greenhouse note section with approximately 3 interview options and specify the timezone the options are given in.
   1. The CES sends the candidate the suggested times using the Candidate Availability for Executive Interviews email template.
   1. After the Candidate responds indicating the time that works for them the CES schedules the interview and uses the interviewer's personal zoom link.
   1. The CES sends the interview confirmation email with a cc to the EBA.

If you have any questions about the process, please post in #eba-team slack channel and @ mention the EBA to the CEO.

#### Interview Schedule

Please ensure you have the [Interview Schedule Chrome Extension](https://chrome.google.com/webstore/detail/interview-schedule/nbimjaonnklighojgkihkipemiaimgdk) installed on your Chrome Browser.

Interview Schedule processes can be found [here](https://about.gitlab.com/handbook/hiring/interview-schedule/).

#### Resource - Candidate Guide

Please ensure you have the [guide chrome extension](https://chrome.google.com/webstore/detail/resource-button/ikeemflbkbdhkgmgbdpeapmnnggbfogd) installed on your Chrome Browser.

Guide processes can be found [here](/handbook/hiring/resource-guide/).

#### If a Candidate Withdraws from the Hiring Process

If a candidate emails the CES Service Desk stating they would like to withdraw from the interview process, the CES should respond to the candidate's email (not the service desk ticket) and cc the recruiter (if the recruiter isn't already on the email). The CES response should not say anything definite as there may be a chance for the recruiter to reel the candidate back in. A basic response is below:

> Hi `Candidate Name`, thank you for your email. I have cc'd your recruiter in this email and if there are any additional questions, they will be in touch.

The CES should also tag the Recruiter in the Greenhouse profile of the candidate who withdrew.

The only exception to this should be for executive roles. Please just forward the withdrawal email to the Executive Recruiter and they will handle any follow-up.

### Reference Checks/Background Checks

GitLab will obtain references and complete a criminal [background check](https://about.gitlab.com/handbook/people-group/people-policy-directory/#background-checks) with employment verifications.

- Once a candidate is moved to the reference check stage, the recruiter should ping `@ces` to kick off the reference check process
- CES will send the Reference Check email via Guide
   - CES will send [references to hiring managers](https://about.gitlab.com/handbook/hiring/talent-acquisition-framework/hiring-manager/#step-19hm-complete-references) with recruiter in cc via email using the "Email the Team" option in GH once received from the candidate. Please ensure this email is sent from the CES team.
- The recruiter should ping @ces once a verbal offer has been made to the candidate. Once the verbal offer has been made, the CES team will initiate the background check process.
- The Candidate Experience Specialist will [initiate a background check](/handbook/people-group/people-policy-directory/#background-checks) and start the contract process for the candidate. CES will continue to monitor the background check until finalized, using the follow-up feature in Greenhouse to ensure the background check is complete and uploaded into BambooHR, if hired
- Background check results will be received by the Candidate Experience Specialist and brought to the relevant People Business Partner for adjudication
- Candidates in **Canada** require a form to be filled out by the employer (GitLab). The candidate will send the form with 2 forms of approved ID (listed on the form) and the CES will verify the name on each form of ID, enter it on the background check form. The CES will enter their signature on the form (either by editing a PDF or staging in DocuSign) and send back to the candidate.
- Employment Verification results will be reviewed by and checked against LinkedIn profiles or CVs by the Candidate Experience Specialist and any discrepancies will brought to the relevant People Business Partner

For additional information on reviewing Background checks see [this page](/handbook/people-group/people-policy-directory/#background-checks).

#### Initiating a Background Check through Greenhouse

**US Candidates Only**

1. Log in to [Greenhouse](https://app2.greenhouse.io/dashboard) and go to the candidate's profile.
1. Click the `Private` tab.
1. Click `Export to Sterling`.
1. Click `Complete Report`, which will redirect you to the Sterling website.
1. Scroll down and click `Add Screening`.
1. Next to `Comprehensive Criminal with Employment`, click on `Ticket`.
1. Select `Add Products` and search for `FACIS® Level 3 Search` and hit Add.
   - If you need to run a financial check as well for Finance team members, you will need to submit a second ticket.
      1. After you submit a ticket for the comprehensive criminal check, navigate back to the candidate's SterlingONE profile.
      1. Click `Add Screening`.
      1. Next to `Federal Criminal District Search` click `Ticket`.
1. Check off that you agree to your obligations as a user.
1. Under `Disclosure and Authorization Options`, select the first option to have Sterling send the candidate a disclosure form.
1. Click `Generate Ticket`.
1. Make a note in the Greenhouse profile that the Background Check has been started

#### Initiating a Background Check through Sterling Talent Solutions

**US Candidates Only**

1. Log in to [Sterling](https://www.talentwise.com/screening/login.php) and select `Quick Launch`.
1. Click `Launch Screening`.
1. Next to `Comprehensive Criminal with Employment` click on `Ticket`.
1. Select `Add Products` and search for `FACIS® Level 3 Search` and hit Add.
1. If you need to run a credit check as well, after you click "Ticket" click "Add Products" on the right and search for "Federal Criminal Check".
1. Check off that you agree to your obligations as a user.
1. Enter the candidate's name and personal email address.
1. Select the first option to have Sterling send the candidate a disclosure form, and click "Generate Ticket".
1. Make a note in the Greenhouse profile that the Background Check has been started

**Non-US Candidates Only**

1. Log in to [Sterling](https://secure.sterlingdirect.com/login/Default.aspx) and E-invite the candidate by inputting their email address.
1. Under "Applicant Information" enter in the candidate's first and last name, as well as their email address to confirm.
1. Next, select "International" from the "Job Position" drop down menu.
1. Next, select "A La Carte" from the "Screening Packing".
1. After that, you will select "Criminal-International". A drop down menu will appear, and you will select the country the candidate is located in. Then click "Add"
1. You'll then select "Verification-Employment (International") and click "Add".
1. If you are submitting a background check for a candidate located in Japan, you will select "GlobeX" instead of "Criminal-International". Then select "Japan" and click "Add"
1. Make sure the criminal check with country of the candidate is included in the "Search" box.
1. Finally, scroll to the bottom of the page and click "Send"
1. Make a note in the Greenhouse profile that the Background Check has been started

#### Escalating Criminal Charges or Employment Discrepancies

Once the CES team gets the background checks back, if there are criminal charges or any employment verification discrepancies - the CES team will escalate to the People Business Partners.

1. Gather all details you can from the Sterling report
1. Post (making sure to tag the [appropriate PBP](https://about.gitlab.com/handbook/people-group/#people-business-partner-alignment-to-division)) all details in the shared Slack channel with the PBP's `employment-criminal-escalations` with PDF version of the report
   - Make sure to include candidate's name, the job applied for, and who the hiring manager is
1. The People Business Partners have their own criteria/process for providing their recommendation on how to proceed to the business
1. Once the PBP has come to a recommendation, they will share the details with that recommendation to the hiring manager
   - It is ultimately up to the business on whether they would like to proceed with the candidate
1. If approval to move forward is given, we will upload the approval to the BambooHR profile once created
1. If approval is not given to move forward, the CES will loop in the Recruiter to make sure the decision and why is communicated correctly to the candidate

### Speaking with TMRG members in the hiring process

Our hiring process includes an **optional** step where candidates can request to meet with an TMRG team member. Candidates can request this at any time throughout the process, we will also proactively offer this to a candidate when they reach the reference check stage. Whether or not the candidate decides to take us up on this offer will have no impact on our overall hiring decision.

When a candidate requests to meet with an TMRG team member, their Candidate Experience Specialist will do the following:
1. Share a message in the respective TMRG Slack channel. To aide with scheduling, the message will include the candidate’s time zone and a request for volunteers who would be willing to speak to that person for a 25-minute Zoom call.

**Suggested Language**

Hi, All! We're looking for a volunteer who would be willing to speak to a candidate for a 25-minute Zoom call. The candidate is located in <LOCATION>. More information regarding the purpose of this call can be found [here](https://about.gitlab.com/company/culture/inclusion/recruiting-initiatives/#speaking-with-tmrg-members-in-the-hiring-process). If you’d like to volunteer, please let me know and provide me with your Calendly link to send to the candidate. Thank you!

   - Below are the slack channels you can ask for volunteers in
      - #lgbtq
      - #women
      - #minoritiesintech
      - #diverse_ability
      - #gender-minorities-employee-resource-group
      - #generational_understanding

2. Once a volunteer has been found the Candidate Experience Specialist will send the candidate the 'TMRG Opportunity' email template. The CES will need to get the TMRG members’ Calendly link and GitLab team page profile.

3. If a volunteer has not been found within 24 hours the CES team member will reach out to the [TMRG lead(s)](/company/culture/inclusion/erg-guide/) and request assistance with scheduling. If a volunteer has not been found within 2 business days of the request, the CES will ask the TMRG lead to take part in the conversation.

As a GitLab team member taking part in these calls, we advise you to start with a short introduction to you and your role here at GitLab. From here, we advise you to let the candidate lead the conversation as the goal is for you to answer their questions and offer insight into how we work.

These calls don’t require you to submit a scorecard in Greenhouse. If a candidate mentions something that you see as a red flag (e.g. they outline a past action of theirs that goes against our values) or share something that would help us set them up for success, we advise you to share the details of this with the hiring manager for the role they’re interviewing for. It will be the responsibility of the Hiring Manager to review this and decide whether we need to alter the hiring or offer process for the candidate.


### Send contract

[See Candidate Experience Specialist Contract Processes section of the handbook](/handbook/hiring/talent-acquisition-framework/ces-contract-processes)

The [Candidate Experience Specialists](/job-families/people-ops/candidate-experience/)) will prepare the contract. While the Candidate Experience Specialist will prioritize a contract above other tasks, the expected turn around on the task is 1 business day. If the contract is time-sensitive, please provide context for the rush. If the Candidate Experience Specialist cannot meet the 1 business day they will inform the recruiter via Greenhouse and will provide context.

Recruiters should make themselves familiar with the basic knowledge of the contract processes that can be found on the [CES Contract Processes](https://about.gitlab.com/handbook/hiring/talent-acquisition-framework/ces-contract-processes/#framework-coord) page and the [CContracts, Probation Periods & PIAA](/handbook/people-group/contracts-probation-periods/#employment-and-contractor-agreements) page.

   1. Check all aspects of the offer:
      - Do we have the new team members' legal name in their profile?
         -  _It is extremely important to enter the team member's full legal name (as much as it is known before visually seeing a legal ID). Full legal name is usually provided for the [background check](/handbook/people-group/people-policy-directory/#background-checks) process. It is important to be as accurate as possible, including the person's full legal name as well as any accents used for their name. This reiterates our Diversity, Inclusion and Belonging values and also prevents audit fails down the line when the [People Experience Associates](https://about.gitlab.com/job-families/people-ops/people-experience-associate/) onboard the team member, and the [People Compliance Specialist](https://about.gitlab.com/job-families/people-ops/people-compliance/) audits all personal data._
      - Is the new team members' address listed on the details page?
      - What contract type and entity are required based upon location and offer details?
      - Is it clear how many (if any) stock options this person should receive?
      - Is all necessary information (start date, salary, location, etc.) up to date?
      - Does the new team member need a work permit or visa, or require an update to them before a start date can be agreed?
      - Has the signatory been determined by the Candidate Experience Specialist and updated?
      - Has the Entity been selected based on the New Hire's location?
   1. [Generate the contract within Greenhouse](/handbook/hiring/talent-acquisition-framework/ces-contract-processes) using a template based on the details found in the offer package.
   1. Contact the recruiter or new team member to gather any missing pieces of information (note: the address can be found on the background check information page).
   1. The Signatory will be either the Talent Acquisition Manager, VP of Talent Acquisition, Chief People Officer, or the CES for PEOs. This can be determinted by the Candidate Expereince Specialist sending a message to the Contracts to Sign channel in Slack.
   1. The entity will be selected based on the new hire's location.
   1. Ensure that, if the contract was created outside of Greenhouse, the contract has been reviewed and approved by the Senior Director of Legal Affairs or a Total Rewards Analyst.
   1. [Stage the contract in DocuSign from within Greenhouse](/handbook/hiring/talent-acquisition-framework/ces-contract-processes), which emails the contract to the signing parties, with the recruiter, talent acquisition manager, and the hiring manager cc'd. It will be sent to the designated signatory as previously determined in Offer Details.
   1. When the contract is signed by all parties, the Candidate Experience Specialist will verify that the start date in Greenhouse is correct.
     - Ensure the candidate has completed the PIAA section with either a `Yes` and the specific details **or** `None`. (Its important that it is not just a GitLab or Github link and more specific info.)
     - If the candidate has specified a `Yes` and the specific details, make the People Experience team aware in our private group slack channel to kick off approval process.
   1. **Before marking the candidate as hired** the Candidate Experience Specialist will reject the candidate from any other active roles including roles for which they are a prospect (without sending a rejection email). _NOTE: If this step is skipped, the profile will not be exported to Bamboo HR when the candidate is marked as hired._
   1. **Before marking the candidate as hired** on Internal candidates, the CES will make sure that the profiles were merged (indicated if the candidate shows their original `Hired` inactive role). _NOTE: If this step is skipped, it will create a duplicate profile exported to Bamboo HR when the candidate is marked as hired._ 
   1. **Before marking the candidate as hired** the CES will ping the recruiter and give them 24 hours to contact/reject all other active candidates. Once this is complete, the CES can proceed with hiring in GH.
   1. **Before marking the candidate as hired** the CES will verify if the listed Recruiter and Coordinator in the `Details` >  `Source & Responsibility` section of the candidate's profile is correct to ensure accuracy in reporting.
   1. The CES will mark the candidate as "Hired" in Greenhouse: _when prompted, select the option to close the req._ Please note, the new hire's BambooHR profile will be generated automatically by the [Greenhouse to BambooHR sync](/handbook/people-group/engineering/gh-bhr-sync/) that runs every 15 minutes.
   1. Once the new hire's profile in BambooHR is generated, The Candidate Experience Specialist will upload the signed contract and the completed background check into the BambooHR profile.
   1. The Candidate Experience Specialist will send an email to total-rewards@gitlab with any variations in contract language (for example a draw). Compensation will sync with Payroll and Sales Ops for any necessary notifications on payment types.
   1. The Candidate Experience Specialist will email the new team member the Welcome Email from Greenhouse with a cc to IT Ops, the Hiring Manager and the Recruiter.  For new team members in USA, use 'GitLab Welcome - US only' template.  For team members located outside the US, use 'GitLab Welcome - non US' template
      * Instructions on the [Notebook Ordering Process](/handbook/business-ops/team-member-enablement/onboarding-access-requests/#laptops) are included with this email.
   1. Should the start date change after the welcome email is sent please see the required steps [here](/handbook/hiring/talent-acquisition-framework/ces-contract-processes/#how-to-update-a-start-date-after-the-contract-is-signed).
   1. Exception to the start date and onboarding date alignment: If a new team member requires a specific start date for legal reasons (cannot have break in employment) but the People Experience Team cannot start onboarding on that specific day (because of Public Holiday), the Candidate Experience Specialist can notify the People Experience team in the private Slack channel `people-exp_ces`. The Contract, Greenhouse and BambooHR should reflect the same start date regardless of the actual onboarding date.  

It is **important** that the Candidate Experience Specialist notifies the People Experience Team of any important changes regarding the new team member, which also ensures the new team members are handed off properly to the People Experience Team.

### Mid-Point Check-In Follow up

The Candidate Experience Specialist will set up a delay send email using the "GitLab Helpful Links - Checking In" email template in GreenHouse. The delay should be set up for halfway between the contract being signed and the start date.
   1. Navigate to the Candidate's profile in GreenHouse.
   1. Under Tools Email Candidate
   1. From the Template drop-down select: GitLab Helpful Links - Checking In
   1. From the Send email when drop-down select: Pick a custom time...
   1. From the calendar select a future date and time, halfway between now and the Candidate's start date.
   1. Check Schedule Email.

If necessary you may cancel the email, which is now showing under the Email Candidate option.

### Next Steps

People Experience Associate will create the onboarding issue and start the [onboarding tasks](/handbook/people-group/general-onboarding/onboarding-processes/) no later than one week before the new team member joins. Should a contract not be signed prior to 5 working days from the start date, a new start date will be required.

For questions about the new team member's onboarding status, you can @mention them in the `#peopleops-confidential` Slack channel.

For questions about the new team member's laptop, ping [IT Ops](#it-ops) in Slack. If the questions arise through email, forward the email to itops@gitlab.com and ping IT Ops in #it-ops Slack, and @it-ops-team too due to volume.

### Interview Reimbursement Process

For candidates requesting [interview reimbursment](/handbook/hiring/interviewing/#reimbursement-for-interviewing-with-gitlab) the CES team will partner with the Account Payable (AP) team to ensure requests are processed confidentially and in a timely manner. AP and the CES team utilize [GitLab Service Desk](/product/service-desk/) to track incoming emails to the Interviews@gitlab.com email.

Under the [Interview Reimbursement Service Desk](https://gitlab.com/interview-reimbursement/ap-ces/issues) set up the proper notifications
   - Click on the bell icon on the top right next to Star and Clone
   - Go to Custom Settings
   - Check "New issue"
   - Closeout the window

Additional process details can be found on the [project README page](https://gitlab.com/interview-reimbursement/ap-ces/blob/master/README.md).

### Employment Offboarding

When employees are offboarding, People Experience will create an offboarding issue. Candidate Expereince is responsible for completing the CES section under Recruiting Operations". Once these steps have been completed, unfollow the issue to disable notifications.
