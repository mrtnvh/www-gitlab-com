---
layout: handbook-page-toc
title: Manager Challenge
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

Welcome to the GitLab Manager Challenge page. The Learning and Development Team has iterated on the program and designed a new curriculum that incorporates elements of [Leadership](/handbook/leadership/) and the book [High Output Management](/handbook/leadership/#books/). 

L&D will be rolling out a **two-week iteration of the program** during the month of May 2021. **The program will start 2021-05-03 and end on 2021-05-14. If you are interested in participating in May, you can [sign up on this issue](https://gitlab.com/gitlab-com/people-group/learning-development/challenges/-/issues/55)**. Aspiring leaders and managers are welcome to sign up for the May program! Currently, the program is being offered to GitLab team members only. 

Please note that if you plan on participating you will need to dedicate at least 20 to 30 minutes each day on the content as well as attend the 1 hour and 20 minute live learning sessions or complete the async live learning activities. Blocking your calendar each day during the program goes a long way!   

One of the biggest benefits of completing the program is **Learning From Other Managers** across GitLab. Networking and async collaboration with learning is a central theme throughout the two-weeks. 

We rolled out the first iteration of the [Week Manager Challenge in September 2020](https://gitlab.com/groups/gitlab-com/people-group/learning-development/-/epics/1), "The GitLab Manager Challenge Program." The pilot received a [Net Promoter Score of 4.6](https://drive.google.com/drive/folders/11J1b8jwxROFYH4Xgylkhgo6x3OEm4kyB?ths=true) by the participants. The [second iteration](https://gitlab.com/groups/gitlab-com/people-group/learning-development/-/epics/26) was run in January 2021 where we condensed the program to three weeks. For May, L&D iterated again and are rolling out a two week program.

All details relating to the next iteration of the Manager Challenge program can be found on this page. 

<figure class="video_container">
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRAwJfDTpSrjif5SxoE7Wsge3D501ENSyHucL4xmORypSSlsdCPIReeV3tKaiCinv_G7yHIKQh5ljA6/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
</figure>

### Manager Challenge Learning Objectives

You may be asking yourself what you will achieve by taking part in the Manager Challenge program! Our goal is that participants walk away with the following skills after completing the four-week challenge: 
- Improve your team performance by strengthening management skills
- Develop a management style that incorporates a whole-person approach to leading others
- Apply and learn GitLab Leadership best practices into your management role
- Demonstrate the application of High Output Management principles 
- Evaluate difficult management situations with greater certainty in your decisions
- Practice emotional intelligence to improve leadership and build more effective relationships with your people
- Build an inclusive environment for your people that is built on trust

Below is a video L&D recorded with Sid on manager enablement: 
<!-- blank line -->
<figure class="video_container">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/jsWDGiSUsA0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>
<!-- blank line -->

### Overview

**What** 

The Manager Challenge Program is a blended learning approach that incorporates self-paced daily challenges and live learning sessions to build foundational management skills. The program incorporates leadership assessments, interactive live learning sessions, and digital learning. 

**Why** 

The program is intended to build a set of baseline skills that complement our values to enable Managers to lead teams at GitLab. 

**How will it help**

Learn the basic principles of what it means to be a manager using a whole-person approach to leadership. The curriculum was built using principles from the book High Output Management, interviews with senior leaders, and GitLab management practices. 

**What do I need to do**

Set aside time each day to participate in the challenge (20-30 minutes) and live learning sessions (1 hour 20 minutes). Complete the weekly learning evaluations, comment on the challenge issues, and fill your individual growth plan. If you are unable to prioritize the training for the 2 weeks, please consider signing up for a session at a later date or discuss additional options with the L&D team. In addition, there will also be async learning options for participants that are unable to attend the live learning sessions. 

### Challenge Issue explainer video 

See the video below for a quick overview of how we use GitLab to facilitate manager enablement 

<!-- blank line -->
<figure class="video_container">
     <iframe width="560" height="315" src="https://www.youtube.com/embed/VTMQiCgvEDg" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>
<!-- blank line -->

### Pre-Challenge Kickoff Call

Before the challenge is offically kicked off, a kickoff meeting will take place to get to know one another better. A roundtable discussion and intro's will take place. The program will be reviewed in depth and questions will be answered. The call is optional but everyone is invited to attend! 

### Week 1

During week one we will discuss **The Art of Management** at GitLab. Information covered in week one can be found on the following handbook pages:  
- [Leadership ](/handbook/leadership/)
- [Building High Performing Teams](/handbook/leadership/build-high-performing-teams/)
- [SOCIAL STYLES](/handbook/leadership/emotional-intelligence/social-styles/)
- [All Remote Management](/company/culture/all-remote/management/)
- [Building Trust](/handbook/leadership/building-trust/)
- [Effective Delegation](/handbook/leadership/effective-delegation/)

| Day     | Challenge Topic | Length of Time   |  Format | Activity |
|---------|-----------------|------------------|---------|---------|
| Day 1    | Getting to know yourself and team members | 20 minutes | Self-paced Daily Challenge | <br> *Learn about the SOCIAL STYLES assessment <br> *Define what it means to be a manager at GitLab |
| Day 2    | Managerial output, leverage, and delegation | 20 minutes | Self-paced Daily Challenge | <br> *Share how to improve mangerial productivitiy |
| Day 3    | Building high performing teams | 20 minutes | Self-paced Daily Challenge | <br> *Explore & determine where your team is in the high performance model <br> *Define how to become a high performing team |
| Day 4    | Live Learning - The Art of Management and Building High Performing Teams | 1hr 20 minutes | Live Learning | <br> *Group excercise to develop a high performing team <br> *Networking with other managers at Gitlab |
| Day 5    | Manager action plan & weekly evaluation | 20 minutes | Self-paced writing & evaluation | <br> * Fill in the Individual Growth Plan based on the week's activities and self-reflection <br> *Complete the weekly evaluation <br> *Catch up on weekly challenges

### Week 2

During week two we will discuss **Effective Communications with Your Team**. Information covered in week two can be found on the following handbook pages and external sites: 
- [Leadership ](/handbook/leadership/)
- [1-1](/handbook/leadership/1-1/)
- [Guidance on Feedback](/handbook/people-group/guidance-on-feedback/)
- [GitLab Communication](/handbook/communication/)
- [Crucial Conversations](https://www.goodreads.com/book/show/15014.Crucial_Conversations)
- [Embracing Asynchronous Communication](/company/culture/all-remote/asynchronous/)

| Day     | Challenge Topic | Length of Time   |  Format | Activity | 
|---------|-----------------|------------------|---------|---------|
| Day 6   | Running an Effective 1:1 (Frequency, format, topics, etc) | 20 minutes | Self-paced Daily Challenge | <br> *Share tips on how to make a 1-1 successful <br> *Define difficult 1:1 topics <br> *Giving & Receiving Feedback <br> Crucial Conversations |
| Day 7   | Cultures | 20 minutes | Self-paced Daily Challenge | <br> * Cross-Cultural Collaboration Activity |
| Day 8   | Coaching skills | 20 minutes | Self-paced Daily Challenge | <br> *Present how and where to coach |
| Day 9   | Live Learning - Effective Communications with Your Team | 1 hr 20 minutes | Live Learning | <br> *Live learning that includes Social Learning activity with role playing on crucial conversations, a bias for async, feedback,  and 1-1's |
| Day 10  | Individual Growth Plan & weekly evaluation | 20 minutes | Self-paced writing & evaluation | <br> * Fill in the Individual Growth Plan based on the week's activities and self-reflection <br> *Complete the weekly evaluation <br> *Catch up on weekly challenges <br> <br> *Share Manager Challenge [certification on LinkedIn](/handbook/people-group/learning-and-development/certifications/#sharing-your-certificate/)! |

### Individual Growth Plan

Throughout the program, we ask that managers to create or refine their [Individual Growth Plan](https://about.gitlab.com/handbook/people-group/learning-and-development/career-development/#internal-resources-1). We encourage Managers to self-reflect on their professional development as a people leader and identify areas where they can advance their skills. 
The document is intended to be a starting point for managers to document where they can improve as a people leader.

### Buddy Program

For the May Manager Challenge, a buddy program will be encouraged for Managers during the event. Managers can select their buddies and complete the async activities together. It can also serve as an opportunity for Managers and team members participating to get to know one another. 

### Post Manager Challenge

L&D is continuing to iterate on continuing education opportunities to build management skills for leading remote teams at GitLab. For the January 2021 Challenge, we will be piloting [Trust Metryx](https://www.xmetryx.com/) to participants that are interested in trying out the tool. 

Trust Metryx is a behavioral change software that accelerates the development of remote team leaders. It helps leaders build trust and create lasting change by reducing friction, lowering stress, and improving performance. Checkout the [three-minute video](https://vimeo.com/493930605) to learn more about Trust Metryx. 

The tool accelerates your development as a manager/leader by: 
- Helping you to identify behaviors that get in the way of team performance wellbeing
- Making it easy for leaders to excel at guiding your team to create the small behavioral nudges that drive continuous improvement
- Enabling you to track your progress creating an inclusive team culture built on trust by providing a specific measurable outcome in terms of team relationship strength
- Also includes **Coach in the Cloud** - a library of behavioral science-based micro-courses and team relationship coaching. Coach in the Cloud helps you develop the skills to navigate relationship challenges and lead with Trust. 

TrustMetryx compliments and builds on the basic principles of what it means to be a manager using the whole-person approach to leadership covered in the Manager Challenge. TrustMetryx gives you a data driven approach to ensuring that you’re embracing GitLab values and creating an inclusive team culture built on trust.

**What do I need to do?**
Learn more about Building Trust with Remote Teams from the co-founder of Trust Metryx by watching the recording of the [Learning Speaker Series](https://www.youtube.com/watch?v=hHMDY77upAE&feature=emb_title). If interested in participating please let the L&D team know in the January Manager Challenge Slack Channel, `#january-2021-manager-challenge.` Review [the onboarding plan](https://drive.google.com/file/d/1S1ynUb8bwpit-e03IP1e6uURYzOgI5KF/view?usp=sharing) for the use of Trust Metryx. 

## Self-paced Learning

Team members have access to [LinkedIn Learning](https://about.gitlab.com/handbook/people-group/learning-and-development/linkedin-learning/) for access to self-paced learning content. The Learning & Development team has developed a learning path in GitLab Learn with curated self-paced content that can benefit [people leaders](https://gitlab.edcast.com/channel/leadership-development) to access anytime. 

## FAQs (Frequently Asked Questions) for the Manager Challenge program

1. **What is the Manager Challenge Program?**
     - The program is intended to build a set of baseline management skills through micro habits and daily practices over 2 weeks. It is a blended learning approach that combines self-paced and live learning sessions. It is intended to build leadership and management skills based on the book High Output Management and GitLab remote management & leadership best practices. 
1. **Do I have to participate?**
     - We strongly recommend that all managers that sign up for the program complete the daily challenges and actively participate. All new managers are strongly encouraged to participate in their first year of leading teams. 
1. **Have we done this program in the past?**
    - We launched the pilot in September 2020. We delivered the second iteration in January 2021.
1. **What is required of me?**
     - Monday to Wednesday there will be a series of daily challenges that a manager can complete asynchronously on their own time that will cover a range of topics. On Thursday's we will come together as a group in a live learning session to apply the challenges to real scenarios managers face in a remote environment; as well as network as a cohort! 
1. **Is there an async option for the live learnings?**
     - Yes, the program will include async learning options for those that are unable to make the live learning sessions. 
1. **I have a lot of management experience, will this help?**
     - No matter your level or tenure as a manager, we can all take time out of our busy days to grow our management and leadership skills. The more practice the better and we hope that you will find the program applicable to your job.
1. **What skills will I be building?**
     - You will be building a range of skills that include: coaching, managing performance, crucial conversations, feedback, building an inclusive culture, developing high performing teams, getting to know your team, and much more!  
1. **How does this complement our values?**
     - All of the challenges will reinforce our values by applying management techniques to lead teams in a remote environment. In the weekly charts above, each day or topic will have a [values competency](/handbook/competencies/#list) that you can expect to improve upon. 
1. **Is the content in the slides in the Handbook?**
     - Yes! All of the content will be in the Handbook. We are creating Google Slides to visualize the content for the challenges but all of it will live in the handbook. 
1. **What if I can’t attend a Live Learning session or complete a challenge?**
     - If you miss a challenge or live learning session you can go back and complete the challenge anytime. Each SSOT page for material covered will be linked on this page. The live learning recordings will be on the respective SSOT page (ex. the recording for the Week 3 Coaching live learning will be on the [Coaching page](/handbook/leadership/coaching/)). 
1. **What if I am out of the office for part of the program?**
     - As long as you complete the challenge and let the Learning and Development know when you have completed them, it is okay to be out of the office during the program. 
1. **I'm not a Manager, can I still participate?**
     - Yes, you can still participate in the Manager Challenge. If you are an aspiring people leader or a non-offical manager, please sign up! You'll get a ton out of the program even if you do not have any direct reports reporting to you. 
1. **How will participation be tracked?**
     - Each challenge will be in the form of an Issue. The details of the challenge will be laid out in the issue. We ask that participants complete the challenge asynchronously and comment on the issue according to the challenge prompt. 
1. **Will I receive a certification?**
     - Yes! Once you complete all challenges, attend the live learning sessions, and complete the self-reflection activities, you will receive the "GitLab Managers Challenge" certificate.

## Manager Challenge Certification

Following the completion of the GitLab Manager Challenge program, participants will be [awarded a certification](/handbook/people-group/learning-and-development/certifications/)! The certification will be sent to a team member who has completed at least 80% of the daily challenges. Once a team member receives a certification, they can post it on their LinkedIn Profile. 

## Testimonials

What people leaders at GitLab learned during the program: 

1. "It's okay to make mistakes. I'm not ashamed to say this but there are managers that participated in this challenge that I almost idolized and thought wow, they are the perfect manager. But then I hear concerns and fears they have and it makes me realize no one is perfect. We are all just trying to be the best we can be and that's all we can do."
2. "Managing is all about the team, its members, and the results generated by trust and collaboration fostered from its own members. A manager's job is to create an environment where trust and collaboration can thrive, set the vision, and serve the members in removing any obstacles allowing them to grow into the best possible version of themselves."
3. "The handbook has so much content, its easy to forget how much tactical information can be found right at your fingertips."
4. "Team performance is cyclical. Perceived regressions aren't bad, but rather a reflection of change in team dynamics. Look for the types of questions people are asking to know how to respond."
5. "The handbook is a great resource with tons of information on being a manager, having hard conversations, and helping teams grow."
6. "For me, these are good reminders of what are the best practices to adopt as a Manager.  I am always exploring, what are ways we can do tasks better and faster.  With that said, as a manager, we need to be sure my people and others are part of the process."
7. "I learned that there are so many amazing managers here at GitLab. Each of the days' comments were treasure troves into how to approach something differently or new techniques that others have found success with."
8. "If you are struggling with something, chances are that other managers have faced the same challenges, so it's good to connect with others and learn from each other!"
9. "A person's culture has tangible impacts on what they hear. Therefore, tailoring a message needs to account for the knowledge and skills of the person as well as account for their cultural background. In other words, there's no such thing as one-size-fits-all communication."
10. "It's possible to be a great remote manager"

## Learning and Development Team 

### Organizing Epics and Issues 

The Manager Challenge Pilot is set up within the GitLab Tool. We are using Epics, Sub Epics, and Issues. There is one main Epic for the whole program, a Sub Epic for each week, and then an Issue for each day. 

**Example of Manager Challenge Pilot Set Up:**

[Manager Challenge Pilot - Epic](https://gitlab.com/groups/gitlab-com/people-group/learning-development/-/epics/1)

[Week 1 - Manager Challenge Pilot - Sub Epic](https://gitlab.com/groups/gitlab-com/people-group/learning-development/-/epics/2)

[Manager Challenge Project](https://gitlab.com/gitlab-com/people-group/learning-development/manager-challenge)

[Manager Challenge Program Pilot: Day 1 - Issue](https://gitlab.com/gitlab-com/people-group/learning-development/manager-challenge/-/issues/1)

**Learning & Development Team Tasks** 

- A week before the program starts: 
   - Create the Epic: Manager Challenge - [Month] [Year] 
   - Create the Sub Epics for each week: Manager Challenge - Week 1 
- Daily
   - Create the Day's Issue in the [Manager Challenge Project](https://gitlab.com/gitlab-com/people-group/learning-development/manager-challenge) using the [manager-challenge-daily-template](https://gitlab.com/gitlab-com/people-group/learning-development/manager-challenge/-/blob/master/.gitlab/issue_templates/manager-challenge-daily-template.md) 
   - Monitor the issue comments to respond to questions as well as keep track of participation 

### Communication 

The modes of communication during the challenge include Slack and GitLab. Ensure that there is a specific channel in Slack set up for challenge members, as well as a group tag in GitLab. 

Each day, once the issue is opened, add the group tag to the issue so participants receive an email notification that the daily issue is ready. For the pilot, the tag was `@gl-manager-challenge-pilot`. 

The Slack channel for the pilot program was `#manager-challenge-pilot`. Each day, once the issue was opened, a message was posted in the Slack channel with a link to the day's issue. 

### Scheduling 

**Live Learning Sessions** 

Each week of the challenge has one day of live learning sessions. Depending on the locations of the participants, it can be determined how many sessions are held on the Thursday of each week.  

Approximate times for the live sessions to ensure timezone coverage include: 7:30 am PT, 1:00 pm PT, and 6:30 pm PT. 

### Retrospective Sessions

After the challenge has been completed, it is suggested to have a live retrospective where participants can come and share any feedback about the program. The time(s) this is scheduled for can be the same as the live learning sessions were during the challenge. 

For the pilot Manager Challenge program, the Learning & Development team held a [retrospective session](https://docs.google.com/document/d/1ecuYFRA2oMqpXbGeaK-VIB8AeX98dJJ2-W-wcYUzaXg/edit?userstoinvite=nadia%40gitlab.com&ts=5f7eb88b) to discuss with participants how we can iterate and improve the program moving forward. [Anonymous feedback and ratings](https://docs.google.com/spreadsheets/d/1iUqcs-kSmXf1ol_lio7TJYcn7VHEGHrNcacW3KQwyKg/edit#gid=314035882) were captured throughout the pilot challenge. Each week participants filled out a learning evaluation form for the weekly challenges and overall program.  

<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/v_ohUbaRFFw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> 
</figure>


## Pilot Program 

In September 2020, the L&D team piloted the Manager Challenge to a group of People Leaders in People Success, Engineering, and Product. Learn more about the pilot content by [reviewing the pilot epic](https://gitlab.com/groups/gitlab-com/people-group/learning-development/-/epics/1). 

Pilot Program: 
- 12 Daily Challenges (20 minutes)
- 4 Live Learning Sessions (1 hour)
- 4 Weekly Self Evaluations and Reflections (10 minutes)
- Certification Upon Completion

Pilot Kickoff Video: 

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/gSMrv9CiqOk" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

## Past Manager Challenges

- [2021 January Manager Challenge](https://gitlab.com/groups/gitlab-com/people-group/learning-development/-/epics/26)
- [2020 September Manager Challenge Pilot](https://gitlab.com/groups/gitlab-com/people-group/learning-development/-/epics/1)

