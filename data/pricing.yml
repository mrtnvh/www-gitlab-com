plans:
  - id: free
    level: 0
    title: Free
    description: A complete DevOps platform
    sub_description: for Getting Started
    monthly_price: 0
    reason: Experience end-to-end DevOps in a single platform
    features:
      - title: All stages of the DevOps lifecycle
      - title: Bring your own CI runners
      - title: Bring your own production environment
      - title: Static application security testing
      - title: 400 CI/CD minutes per month
    features_more:
      - title: Free static websites with GitLab Pages
      - title: Remote repository push mirroring
      - title: Time tracking
      - title: Wiki based project documentation
      - title: Jira Development Panel integration
      - title: Secret detection
      - title: Dependency proxy for container registry
    links:
      hosted: https://gitlab.com/users/sign_up?test=capabilities
      self_managed: /install/?test=capabilities
  - id: silver-premium
    level: 2
    title: Premium
    description: DevOps with project management, code integrity controls, and productivity analytics
    sub_description: for Teams
    monthly_price: 19
    reason: Cross-project visibility and control for DevOps workflows
    features:
      - title: Cross-team project management and roadmaps
      - title: Code integrity controls and approvals
      - title: Code and productivity analytics
      - title: Disaster recovery
      - title: 24/7 support
      - title: 10,000 CI/CD minutes per month
    features_more:
      - title: Push rules
      - title: Multiple approvers in code review
      - title: Efficient merge request reviews
      - title: Code quality reports
      - title: Protected environments
      - title: Merge trains
      - title: Issue weights
      - title: Multiple issue assignees
      - title: Roadmaps
      - title: Single level epics
      - title: Globally distributed cloning with GitLab Geo*
      - title: Advanced search
      - title: Support for Scaled GitLab*
      - title: Packaged PostgreSQL with replication and failover*
      - title: Advanced LDAP/SAML support including Group Sync*
    links:
      hosted: https://gitlab.com/-/subscriptions/new?plan_id=2c92a0fd5a840403015aa6d9ea2c46d6&test=capabilities
      self_managed: https://customers.gitlab.com/subscriptions/new?plan_id=2c92a0ff6145d07001614f0ca2796525&test=capabilities
  - id: gold-ultimate
    level: 3
    title: Ultimate
    description: Full DevSecOps with portfolio management, advanced security testing, and compliance
    sub_description: for Organizations
    monthly_price: 99
    reason: Planning, security, and compliance built-in
    features:
      - title: Vulnerability management
      - title: Company wide portfolio management
      - title: Advanced application security
      - title: Dependency scanning
      - title: Compliance automation
      - title: Free guest users
      - title: 50,000 CI/CD minutes per month
    features_more:
      - title: Create test cases from within GitLab
      - title: Quality management
      - title: Status page
      - title: Security dashboards
      - title: Dependency scanning
      - title: Dynamic application security testing
      - title: Compliance dashboard
      - title: Requirements measurement
      - title: Contribution analytics
      - title: Issue and epic health reporting
      - title: Portfolio-level roadmaps
      - title: Project dependency list
      - title: Group and project insights
      - title: Import & export requirements
    links:
      hosted: https://gitlab.com/-/subscriptions/new?plan_id=2c92a0fc5a83f01d015aa6db83c45aac&test=capabilities
      self_managed: https://customers.gitlab.com/subscriptions/new?plan_id=2c92a0fe6145beb901614f137a0f1685&test=capabilities
questions:
  - question: What are pipeline minutes?
    answer: >-
      Pipeline minutes are the execution time for your pipelines on our shared runners. Execution on your own runners
      will not increase your pipeline minutes count and is unlimited.
  - question: What happens if I reach my minutes limit?
    answer: >-
      If you reach your limits, you can [manage your CI/CD minutes usage](https://about.gitlab.com/pricing/faq-consumption-cicd/#managing-your-cicd-minutes-usage),
      [purchase additional CI minutes](https://customers.gitlab.com/plans), or
      upgrade your account to Premium or Ultimate. Your own runners can still be used even if you reach your limits.
  - question: Does the minute limit apply to all runners?
    answer: >-
      No. We will only restrict your minutes for our shared runners. If you have a
      [specific runner setup for your projects](https://docs.gitlab.com/runner/), there is no limit to your build time
      on GitLab SaaS.
  - question: Can I acquire a mix of licenses?
    answer: >-
      No, all users in the group need to be on the same plan.
  - question: Are GitLab Pages included in the free plan?
    answer: >-
      Absolutely, GitLab Pages will remain free for everyone.
  - question: Can I import my projects from another provider?
    answer: >-
      Yes. You can import your projects from most of the existing providers, including GitHub and Bitbucket.
      [See our documentation](https://docs.gitlab.com/ee/user/project/import/index.html) for all your import
      options.
  - question: I already have an account, how do I upgrade?
    answer: >-
      Head over to [https://customers.gitlab.com](https://customers.gitlab.com), choose the plan that is right for you.
  - question: Do plans increase the minutes limit depending on the number of users in that group?
    answer: >-
      No. The limit will be applied to a group, no matter the number of users in that group.
  - question: How much space can I have for my repo on GitLab SaaS?
    answer: >-
      10GB per project.
  - question: Can I buy additional storage space for myself or my organization?
    answer: >-
      Yes, you can purchase additional storage for your group on the [GitLab customer portal](https://customers.gitlab.com/). 
  - question: Do you have special pricing for open source projects, educational institutions, or startups?
    answer: >-
      Yes! We provide free Ultimate licenses, along with 50K CI minutes/month, to qualifying open source projects, educational institutions, and startups. Find out more by visiting our [GitLab for Open Source](/solutions/open-source/), [GitLab for Education](/solutions/education/), and [GitLab for Startups](/solutions/startups/) program pages.
  - question: How does GitLab determine what future features fall into given tiers?
    answer: >-
      On this page we represent our [capabilities](/company/pricing/#capabilities) and those are meant to be filters on our [buyer-based open core](/company/pricing/#buyer-based-tiering-clarification) pricing model. You can learn more about how we make tiering decisions on our [pricing handbook](/handbook/ceo/pricing) page.
  - question: Where is SaaS hosted?
    answer: >-
      Currently we are hosted on the Google Cloud Platform in the USA
  - question: What features do not apply to GitLab SaaS?
    answer: >-
      Some features are unique to self-managed and do not apply to SaaS. You can find an up to date list [on our why GitLab SaaS page](/handbook/marketing/strategic-marketing/dot-com-vs-self-managed/index.html).
  - question: What is a user?
    answer: >-
      User means each individual end-user (person or machine) of Customer and/or its Affiliates (including, without
      limitation, employees, agents, and consultants thereof) with access to the Licensed Materials hereunder.
  - question: Can I add more users to my subscription?
    answer: >-
      Yes. You have a few options. You can add users to your subscription any time during the subscription period. You
      can log in to your account via the [GitLab Customers Portal](https://customers.gitlab.com) and add more seats or by
      either contacting [renewals@gitlab.com](mailto:renewals@gitlab.com) for a quote. In either case, the cost will be
      prorated from the date of quote/purchase through the end of the subscription period. You may also pay for the
      additional licences per our true-up model.
  - question: The True-Up model seems complicated, can you illustrate?
    answer: >-
      If you have 100 active users today, you should purchase a 100 user subscription. Suppose that when you renew next
      year you have 300 active users (200 extra users). When you renew you pay for a 300 user subscription and you also
      pay the full annual fee for the 200 users that you added during the year.
  - question: How does the license key work?
    answer: >-
      The license key is a static file which, upon uploading, allows GitLab Enterprise Edition to run. During license
      upload we check that the active users on your GitLab Enterprise Edition instance doesn't exceed the new number of
      users. During the licensed period you may add as many users as you want. The license key will expire after one
      year for GitLab subscribers.
  - question: What happens when my subscription is about to expire or has expired?
    answer: >-
      You will receive a new license that you will need to upload to your GitLab instance. This can be done by following
      [these instructions](https://docs.gitlab.com/ee/user/admin_area/license.html).
  - question: What happens if I decide not to renew my subscription?
    answer: >-
      14 days after the end of your subscription, your key will no longer work and GitLab Enterprise Edition will not be
      functional anymore. You will be able to downgrade to GitLab Community Edition, which is free to use.
