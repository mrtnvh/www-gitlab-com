title: Anchormen
cover_image: '/images/blogimages/anchormen.jpg'
cover_title: |
  How GitLab CI/CD supports and accelerates innovation for Anchormen
cover_description: |
  Data engineering and AI consultancy, Anchormen, achieves great customer success, excellent workflow efficiency, and reliable continuous deployment with GitLab.
twitter_image: '/images/blogimages/anchormen.jpg'

twitter_text: 'Learn how GitLab CI/CD supports and accelerates innovation for @anchormenBDS with 85% reduction in deployment time.'

customer_logo: '/images/case_study_logos/anchormen-logo.png'
customer_logo_css_class: brand-logo-tall
customer_industry: Technology
customer_location: Amsterdam and Groningen
customer_solution: GitLab Starter
customer_employees: 70 
customer_overview: |
  GitLab’s intuitive interface and superior integration empowers Anchormen to deliver quality solutions on schedule.  
customer_challenge: |
  Anchormen wanted a more professional workflow in order to better integrate and collaborate internally and with clients. 

key_benefits:
  - |
    Integration with AWS, Azure, Jira, SonarQube
  - |
    Cost effective
  - |
    Improved collaboration
  - |
    Easy to use interface
  - |
    Seamless Docker deployment
  - |
    Operational efficiency
  - |
    Improved solution deployment

customer_stats:
  - stat: 85%  
    label: Deployment time reduction
  - stat: 84    
    label: Software projects in GitLab
  - stat: 3.5    
    label: Times team growth


customer_study_content:
  - title: the customer
    subtitle: Modern data engineering and AI consultancy
    content:
      - |
        Anchormen is a machine learning, data science and engineering consultancy based in The Netherlands. <a href="https://anchormen.nl" target="_blank">Anchormen</a> builds and develops innovative data and AI solutions that give clients meaningful insights for their business. A lot of the data, AI, and ML solutions that Anchormen deploys require solid software engineering work.
      
  - title: the challenge
    subtitle: Moving away from Jenkins
    content:
      - |
        Anchormen was using Jenkins, but in 2015 the development teams decided that they needed a more professional workflow. Anchormen had an urgent need for merge requests and the processes in place didn’t support the collaborative workflows they needed to be effective. The teams were also using Rancher for some projects, but weren’t pleased with the updates and decided to adopt Kubernetes.   
      - |
        Because Anchormen caters to a variety of companies, quality collaboration with clients is essential for delivering excellent results. Anchormen focuses on data engineering, so they need to be able to work in an environment that is like a playground for customers to be able to play and innovate in as well. 
      - | 
        Anchormen required a tool that could [integrate with AWS](https://about.gitlab.com/partners/technology-partners/aws/), Azure, Docker, Jira, and SonarQube. They also wanted to ensure that product delivery could happen seamlessly and on time. “We're not a software company that has one long-term project. Our typical project lasts between half a year and a year, and then we hand it over to the customer, with us taking a supporting role if necessary,” according to Jeroen Vlek, CTO of Anchormen.
    
  - blockquote: GitLab has really helped us because we can work with a lot of people on projects, maintain the quality, make sure that everybody checks each other's work, and then deploy it on an infrastructure that doesn't bear any surprises because it just follows through the continuous deployment. It just follows wherever the project is going.
    attribution: Jeroen Vlek 
    attribution_title: CTO 

  - title: the solution
    subtitle: Easy interface, integration, and source control
    content:
      - |
        Anchormen adopted GitLab in 2015, and one of the big selling points was the easy and intuitive user interface. “It's easy to use, it's logical. If I'm looking for something in GitLab, I never have to Google it. I usually just find my way rather quickly through the interface. That's a big plus. That's not what every tool can say,” according to Vlek. “I really like that GitLab's build definitions are part of the project, so that's something that can evolve with the project as it should be.”
      - |
        Anchormen also liked the ability to deploy runners. Dockerized runners with any underlying software means that there are no dependencies. Every build is isolated. On top of that, Anchormen had SCM prior to adopting GitLab, but the platform added another, easier layer. “This is an added layer on top of version control. So, it makes everything a lot more manageable, and it really helps you control the quality of the work, which of course, version control by itself doesn't necessarily do,” Vlek added.  

  - title: the results
    subtitle: CI/CD ease, product improvement, customer satisfaction 
    content:
      - |
        Overall, development teams are happy with the GitLab adoption. In particular, teams appreciate the configurability and ease of integration with other tools. Teams can define their own integration and deployment builds through a GMO file in the repository and then they can configure from there. “What we also really like is the LDAP integration. We have it with our access directory which makes user management very simple. And then adding users to customer groups for example, is very straightforward,” Vlek said. 
      - |
        Anchormen [integrates Jira with GitLab](https://about.gitlab.com/solutions/jira/). Every commit message starts with a Jira ticket number, then Jira establishes the link between the ticket and the commits in GitLab. This integration works well for the teams because if there is any progress on the ticket, or if it’s a historical ticket, they can look back to see what happened and what work has already been done. This visibility has improved the workflow and developers appreciate the transparency it delivers.   
      - |
        The development teams use Azure and AWS integration with GitLab. They have Cloud Formation templates for AWS as part of the [CI/CD pipeline](https://about.gitlab.com/blog/2019/07/12/guide-to-ci-cd-pipelines/) so that the infrastructure follows GitFlow through GitLab. From there, GitLab automatically pushes the cloud formation templates to AWS to update the environment. Anchormen integrates SonarQube in the CI/CD pipeline for automatic code inspection.  
      - |
        GitLab has transformed Anchormen’s workflow processes and they now have over 80 software projects within the platform. “I don't even know how we used to manage without it. I think around 2015, we had a small team of around 20 people, and now we're with 70 people. GitLab really helped us to structure the growth in both employees and projects,” Vlek said.
      - |
        Anchormen’s operational efficiency has increased with GitLab as a system in place and deployment time accelerating from 20 minutes down to just three. “You can really focus on the work. You don't need to think about all these other things, because once you've had GitLab set up, you have basically this safety net. GitLab protects you, and you can just really focus on the business logic. I would definitely say it improves operational efficiency,” Vlek added.
      - |
        The product outcome has also accelerated with a consistent and enhanced workflow. “Because of this kind of seamless integration of tests and deployment, there's not a lot of room for human errors anymore. By greatly reducing that risk, you also really increase the quality of your projects,” Vlek said. Anchormen are consistent in keeping their “eyes on the horizon” for where they want a project to go, with a focus on delivering superior business value. 
      - |
        Since working with GitLab, teams have been successful in exceeding customer expectations and reaching deadlines on time. “GitLab has really helped us, because we can work with a lot of people on the projects, maintain the quality, make sure that everybody checks each other's work, and then deploy it on a stable infrastructure which follows through the continuous deployment. It just follows wherever the project is going,” Vlek said.
        
        <b>Learn more about GitLab CI/CD</b> 
      - |
        [Optimize your DevOps value stream](/solutions/value-stream-management/)
      - |
        [Master continuous software development](/webcast/mastering-ci-cd/)
      - |
        [What is a Git workflow](https://about.gitlab.com/topics/version-control/what-is-git-workflow/#)
